import json
import os
from flask import Flask, send_from_directory,request,abort,jsonify
from flask_cors import CORS
from flask_swagger_ui import get_swaggerui_blueprint
from helper import global_variables as gv
from controllers import faces_controller, text_controller, preprocess_controller, sound_controller
from sound_service import services
from functools import wraps


# ============================================================
# --------------------- API KEY AUTH -----------------------------
# ============================================================

def require_appkey(view_function):
    @wraps(view_function)
    # the new, post-decoration function. Note *args and **kwargs here.
    def decorated_function(*args, **kwargs):
        #if request.args.get('key') and request.args.get('key') == key:
        if request.headers.get('x-api-key') and request.headers.get('x-api-key') in gv.APIKEYS:
            return view_function(*args, **kwargs)
        else:
            abort(401)
    return decorated_function
app = Flask(__name__,static_folder=os.path.join(os.getcwd(),'www'))
CORS(app)


# ============================================================
# --------------------- Root Web -----------------------------
# ============================================================
@app.route('/', methods=['GET'])
def root():
	return app.send_static_file('index.html')


# ============================================================
# -----------------Get List of media folders -----------------
# ============================================================

@app.route('/media', methods=['GET'])
def get_media_folders():
	try:
		files=os.listdir(os.path.join("www","media"))
		files_clean=[file for file in files if not file.startswith(".")]
		series=[]
		title_index=0;
		lang=""
		titles={"es":["Como si fuera ayer","No puede ser!","Bienvenidos a la familia","Telenoticias"],"it":["Come se fosse ieri","Non può essere!","Benvenuto in famiglia","Notizie"],"ca":["Com_si_fos_ahir","No_pot_ser!","Benvinguts_a_la_familia","Telenoticies"],"en":["As if were yesterday","It can be!","Welcome to the family","News"]}
		for acceptedLang in request.headers['Accept-Language'].split(","):
				language=acceptedLang.split(";");
				if(language[0][:2] in ["en","es","ca","it"]):
					lang=language[0][:2]
					break
		for serie in files_clean:
			info={}
			info["name"]=serie
			if(serie=="Com_si_fos_ahir"):
				title_index=0
			elif(serie=="No_pot_ser!"):

				title_index=1
			elif(serie=="Benvinguts_a_la_familia"):
				title_index=2
			elif(serie=="Telenoticies"):
				title_index=3
			
			info["title"]=titles[lang][title_index]
			info["thumbnail"]=request.url_root+"media/"+serie+"/thumbnail.jpg"
			series.append(info)
		return json.dumps({"media":files_clean,"series":series})
	except Exception as e:
		gv.logger.error(e)
		abort(404)
# ============================================================
# ----------------------- MediaSearch ------------------------
# ============================================================
@app.route('/media/search', methods=['POST'])
def search():
	try:
		if not request.json or not request.json["query"]:
			abort(400,{"message":"Bad Request"})
		lang=""
		for acceptedLang in request.headers['Accept-Language'].split(","):
			language=acceptedLang.split(";");
			if(language[0][:2] in ["en","es","ca","it"]):
				if(language[0][:2]!="en"):
					lang="_"+language[0][:2]
				break
		query=request.json['query'].split(" ")
		files=os.listdir(os.path.join("www","media"))
		files_clean=[file for file in files if not file.startswith(".")]
		episodes=[]
		for serie in files_clean:
			files_serie=os.listdir(os.path.join("www","media",serie))
			files_Serie_clean=[file for file in files_serie if os.path.isdir(os.path.join("www","media",serie,file)) and file != "Dataset"]
			for file in files_Serie_clean:
				with open(os.path.join("www","media",serie,file,file+"_info"+lang+".json")) as json_file:
					try:
						data = json.load(json_file)
						found=False
						for q in query:
							if data["description"] and  q.lower() in data["description"].lower():
								found = True
							if data["title"] and q.lower() in data["title"].lower() :
								found = True
						if found:
							info={}
							info["title"]=data["title"]
							info["description"]=data["description"]
							info["duration"]=data["duration"]
							info["actors"]=[]
							info["directors"]=[]
							info["services"]=[]
							info["audio_langs"]=data["audio_langs"]
							info["subtitle_langs"]=data["subtitle_langs"]
							if os.path.isfile(os.path.join("www","media",serie,file,file+"_faces_final.json")):
								info["faces_status"]=gv.Status.PROCESSED.value
							elif os.path.isfile(os.path.join("www","media",serie,file,file+"_characters_annotations.json")):
								info["faces_status"]=gv.Status.ANNOTATED.value
							elif os.path.isfile(os.path.join("www","media",serie,file,file+"_faces_annotator.json")):
								info["faces_status"]=gv.Status.WAITINGUSERANOTATION.value
							elif os.path.isfile(os.path.join("www","media",serie,file,"thumbnail.jpg")):
								info["faces_status"]=gv.Status.GENERATINGANOTATIONFILE.value
							else:
								info["faces_status"]=gv.Status.NONPREPROCESSED.value

							#TEXT STATUS
							if os.path.isfile(os.path.join("www","media",serie,file,file+"_text_process.json")):
								info["text_status"]=gv.Status.PROCESSED.value
							elif os.path.isfile(os.path.join("www","media",serie,file,file+"_text_settings.json")):
								info["text_status"]=gv.Status.ANNOTATED.value
							elif os.path.isfile(os.path.join("www","media",serie,file,"thumbnail.jpg")):
								info["text_status"]=gv.Status.WAITINGUSERANOTATION.value
							else:
				 				info["text_status"]=gv.Status.NONPREPROCESSED.value

				 			#SOUND STATUS
							if os.path.isfile(os.path.join("www","media",serie,file,"env_sounds.json")):
								info["sound_status"]=gv.Status.PROCESSED.value
							else:
								info["sound_status"]=gv.Status.NONPREPROCESSED.value
							if os.path.isfile(os.path.join("www","media",serie,file,file+"_faces_final.json")):
								info["services"].append("face-magnification")
								info["services"].append("character-recognition")
							if os.path.isfile(os.path.join("www","media",serie,file,file+"_text_process.json")):
								info["services"].append("text-recognition")
							if os.path.isfile(os.path.join("www","media",serie,file,file+"_sound_events.json")):
								info["services"].append("sounds-recognition")
							info["thumbnail"]=request.url_root+"media/"+serie+"/"+file+"/thumbnail.jpg"
							info["mpd_url"]=request.url_root+"media/"+serie+"/"+file+"/manifest_sbt.mpd"
							mpd_contents = open(os.path.join("www","media",serie,file,"manifest_sbt.mpd")).read()
							if "lang=\"ad\"" in mpd_contents:
								info["services"].append("audio-description") 
							episodes.append(info);
					except Exception as e:
						print(e)
						print("error:"+file)
		if episodes:
			return(json.dumps({"episodes":episodes}))
		else:
			abort(404,{"message":"Not found"})
	except Exception as e:
		gv.logger.error(e)
		print(e)
		abort(e)

# ============================================================
# -----------------Get List of media folders -----------------
# ============================================================

@app.route('/media/<serie>/', methods=['GET'])
def get_media_episodes(serie):
	try:
		files=os.listdir(os.path.join("www","media",serie))
		files_clean=[file for file in files if os.path.isdir(os.path.join("www","media",serie,file)) and file != "Dataset"]
		episodes=[]
		lang=""
		for acceptedLang in request.headers['Accept-Language'].split(","):
				language=acceptedLang.split(";");
				if(language[0][:2] in ["en","es","ca","it"]):
					if(language[0][:2]!="en"):
						lang="_"+language[0][:2]
						
					break
		for file in files_clean:
			##FACE STATUS
			episode={}
			quality=-1;

			
			episode["name"]=file
			if os.path.isfile(os.path.join("www","media",serie,file,file+"_faces_final.json")):
				episode["faces_status"]=gv.Status.PROCESSED.value
			elif os.path.isfile(os.path.join("www","media",serie,file,file+"_characters_annotations.json")):
				episode["faces_status"]=gv.Status.ANNOTATED.value
			elif os.path.isfile(os.path.join("www","media",serie,file,file+"_faces_annotator.json")):
				episode["faces_status"]=gv.Status.WAITINGUSERANOTATION.value
			elif os.path.isfile(os.path.join("www","media",serie,file,"thumbnail.jpg")):
				episode["faces_status"]=gv.Status.GENERATINGANOTATIONFILE.value
			else:
				episode["faces_status"]=gv.Status.NONPREPROCESSED.value

			#TEXT STATUS
			if os.path.isfile(os.path.join("www","media",serie,file,file+"_text_process.json")):
				episode["text_status"]=gv.Status.PROCESSED.value
			elif os.path.isfile(os.path.join("www","media",serie,file,file+"_text_settings.json")):
				episode["text_status"]=gv.Status.ANNOTATED.value
			elif os.path.isfile(os.path.join("www","media",serie,file,"thumbnail.jpg")):
				episode["text_status"]=gv.Status.WAITINGUSERANOTATION.value
			else:
 				episode["text_status"]=gv.Status.NONPREPROCESSED.value

 			#SOUND STATUS
			if os.path.isfile(os.path.join("www","media",serie,file,"env_sounds.json")):
				episode["sound_status"]=gv.Status.PROCESSED.value
			else:
				episode["sound_status"]=gv.Status.NONPREPROCESSED.value


			info=os.path.join("www","media",serie,file,file+"_info"+lang+".json");
			
			with open(info) as json_file:
				data = json.load(json_file)
				if(data["audio_langs"]):
					episode["audio_langs"]=data["audio_langs"]
			
				episode["subtitle_langs"]=data["subtitle_langs"]
				episode["title"]=data["title"]
				episode["description"]=data["description"]
				episode["duration"]=data["duration"]
				episode["thumbnail"]=request.url_root+"media/"+serie+"/"+file+"/thumbnail.jpg"

			episodes.append(episode);
		return json.dumps({"episodes":episodes,"thumbnail":request.url_root+"media/"+serie+"/thumbnail.jpg"})
	except Exception as e:
		gv.logger.error(e)
		abort(404)

# ============================================================
# -----------------Get services of episode -----------------
# ============================================================

@app.route('/media/services/<serie>/<episode>', methods=['GET'])
def get_servicves_media_episodes(serie,episode):
	try:
		lang=""
		for acceptedLang in request.headers['Accept-Language'].split(","):
			language=acceptedLang.split(";");
			if(language[0][:2] in ["en","es","ca","it"]):
				if(language[0][:2]!="en"):
					lang="_"+language[0][:2]
				break

		info=os.path.join("www","media",serie,episode,episode+"_info"+lang+".json");
		with open(info) as json_file:
			data = json.load(json_file)
			info={}
			info["title"]=data["title"]
			info["description"]=data["description"]
			info["duration"]=data["duration"]
			info["actors"]=[]
			info["directors"]=[]
			info["services"]=[]
			info["audio_langs"]=data["audio_langs"]
			info["subtitle_langs"]=data["subtitle_langs"]
			if os.path.isfile(os.path.join("www","media",serie,episode,episode+"_faces_final.json")):
				info["services"].append("face-magnification")
				info["services"].append("character-recognition")
			if os.path.isfile(os.path.join("www","media",serie,episode,episode+"_text_process.json")):
				info["services"].append("text-recognition")
			if os.path.isfile(os.path.join("www","media",serie,episode,episode+"_sound_events.json")):
				info["services"].append("sounds-recognition")
			info["thumbnail"]=request.url_root+"media/"+serie+"/"+episode+"/thumbnail.jpg"
			info["mpd_url"]=request.url_root+"media/"+serie+"/"+episode+"/manifest_sbt.mpd"
			mpd_contents = open(os.path.join("www","media",serie,episode,"manifest_sbt.mpd")).read()
			if "lang=\"ad\"" in mpd_contents:
				info["services"].append("audio-description") 
		return json.dumps(info)
	except Exception as e:
		gv.logger.error(e)
		abort(404)

# ============================================================
# ---------------------Get characters Json -------------------
# ============================================================
@app.route('/characters/<media>/<episode>', methods=['GET'])
@require_appkey
def get_episode_characters(media,episode):
	try:
		with open(os.path.join("www","media",media,episode,episode+"_characters_annotations.json")) as f:
			return json.dumps(json.load(f))
	except Exception as e:
		gv.logger.error(e)
		abort(404)

# ============================================================
# ---------------------Save Characters Json -------------------
# ============================================================
@app.route('/characters/<media>/<episode>', methods=['POST'])
@require_appkey
def post_save_characters(media,episode):
	try:
		if not request.json or not request.json['characters']:
			abort(400,{"message":"Bad Request"})
		with open(os.path.join("www","media",media,episode,episode+"_characters_annotations.json"),'w') as f:
			f.write(json.dumps(request.json['characters']))
		return(json.dumps({"message":"ok"}))
	except Exception as e:
		gv.logger.error(e)
		abort(404,{"message":"Episode not found"})

# ============================================================
# ---------------------Save Events Json -------------------
# ============================================================
@app.route('/sounds/<media>/<episode>', methods=['POST'])
@require_appkey
def post_save_sounds(media,episode):
	try:
		if not request.json or not request.json['events']:
			abort(400,{"message":"Bad Request"})
		with open(os.path.join("www","media",media,episode,episode+"_sound_events.json"),'w') as f:
			f.write(json.dumps({"Output":request.json['events']}))
		return(json.dumps({"message":"ok"}))
	except Exception as e:
		gv.logger.error(e)
		abort(404,{"message":"Episode not found"})
# ============================================================
# --------------Save Text Detection Area Json ----------------
# ============================================================
@app.route('/textdetection/area/<media>/<episode>', methods=['POST'])
@require_appkey
def post_save_text_detection_area(media,episode):
	try:
		if not request.json or "area" not in request.json:
			abort(400,{"message":"Bad Request"})
		with open(os.path.join("www","media",media,episode,episode+"_text_settings.json"),'w') as f:
			f.write(json.dumps(request.json['area']))
		return(json.dumps({"message":"ok"}))
	except Exception as e:
		gv.logger.error(e)
		abort(404,{"message":"Episode not found"})


# ============================================================
# --------------Get Text Detection Area Json ----------------
# ============================================================
@app.route('/textdetection/area/<media>/<episode>', methods=['GET'])
@require_appkey
def get_text_detection_area(media,episode):
	try:
		with open(os.path.join("www","media",media,episode,episode+"_text_settings.json")) as f:
			return json.dumps(json.load(f))
	except Exception as e:
		gv.logger.error(e)
		abort(404)

# ============================================================
# --------------------Preprocess Content----------------------
# ============================================================

@app.route('/preprocess/<media>/<episode>', methods=['POST'])
@require_appkey
def preprocess_content(media,episode):
	try:
		response = preprocess_controller.preprocess(media=media,episode=episode)
		return json.dumps(response)

	except Exception as e:
		gv.logger.error(e)
		abort(404)


# ============================================================
# --------------------Start Face Detection -------------------
# ============================================================

@app.route('/facesdetection/start/<media>/<episode>', methods=['POST'])
@require_appkey
def start_face_detection(media,episode):
	try:
		response = faces_controller.start(media=media,episode=episode)
		return json.dumps(response)

	except Exception as e:
		gv.logger.error(e)
		abort(404)

# ============================================================
# --------------------Face Detection Status ------------------
# ============================================================

@app.route('/facesdetection/status/<media>/<episode>', methods=['GET'])
@require_appkey
def face_detection_status(media,episode):
	try:
		return json.dumps({"message":"ok","status":str(gv.video_status[media][episode])})
		##TODO
			## Return the status of the porcess for that video{ DEtecting, Preprocessing, waiting traingn or Ended}
			## 404 if episode does not exists

	except Exception as e:
	
		gv.logger.error(e)
		abort(404)


# ============================================================
# ------------------ Train Face Detection --------------------
# ============================================================
@app.route('/facesdetection/train/<media>', methods=['POST'])
@require_appkey
def train_face_detection(media):
	try:
		reponse=faces_controller.train(media)
		return json.dumps(reponse)

	except Exception as e:
		gv.logger.error(e)
		abort(404)


# ============================================================
# --------------------Start Text Detection -------------------
# ============================================================

@app.route('/textdetection/start/<media>/<episode>', methods=['POST'])
@require_appkey
def start_text_detection(media,episode):
	try:
		response = text_controller.start(media=media,episode=episode)
		
		return json.dumps(response)

	except Exception as e:
		gv.logger.error(e)
		abort(404)

# ============================================================
# --------------------Text Detection Status ------------------
# ============================================================

@app.route('/textdetection/status/<media>/<episode>', methods=['GET'])
@require_appkey
def text_detection_status(media,episode):
	try:

		return json.dumps({"message":"ok","status":str(gv.text_status[media][episode])})
		##TODO
			## Return the status of the porcess for that video{ DEtecting, Preprocessing, waiting traingn or Ended}
			## 404 if episode does not exists

	except Exception as e:
		gv.logger.error(e)
		abort(404)


# ============================================================
# --------------- Sound Services Status -----------------
# ============================================================

@app.route('/soundanalysis/create_speaker_dataset/<media>', methods=['POST'])
@require_appkey
def call_create_speaker_dataset(media):
	try:
		response = sound_controller.create_speaker_dataset(media)
		return json.dumps(response)
	except Exception as e:
		gv.logger.error(e)
		abort(404)


@app.route('/soundanalysis/train_speaker_model/<media>', methods=['POST'])
@require_appkey
def call_train_model_speaker(media):
	try:
		response = sound_controller.train_speaker_model(media)
		return json.dumps(response)
	except Exception as e:
		gv.logger.error(e)
		abort(404)


@app.route('/soundanalysis/calculate_embeddings/<media>', methods=['POST'])
@require_appkey
def call_calculate_embeddings(media):
	try:
		response = sound_controller.calculate_embeddings(media)
		return json.dumps(response)
	except Exception as e:
		gv.logger.error(e)
		abort(404)


@app.route('/soundanalysis/analyse_speakers_episode/<media>/<episode>', methods=['GET'])
@require_appkey
def call_analyse_speakers_episode(media, episode):
	try:
		response = sound_controller.analyse_speakers_episode(media, episode)
		return json.dumps(response)
	except Exception as e:
		gv.logger.error(e)
		abort(404)


@app.route('/soundanalysis/analyse_sounds_episode/<media>/<episode>', methods=['GET'])
@require_appkey
def call_analyse_sounds_episode(media, episode):
	try:
		response = sound_controller.analyse_env_sound_episode(media, episode)
		return json.dumps(response)
	except Exception as e:
		gv.logger.error(e)
		abort(404)




# ============================================================
# ------------------------- Swagger --------------------------
# ============================================================

SWAGGER_URL = '/docs'
API_URL = '/swagger/swagger.json'
SWAGGERUI_BLUEPRINT = get_swaggerui_blueprint(
    SWAGGER_URL,
    API_URL,
    config={
        'app_name': "Easy TV Services"
    }
)
app.register_blueprint(SWAGGERUI_BLUEPRINT, url_prefix=SWAGGER_URL)



# ============================================================
# --------------------- Static web files ---------------------
# ============================================================
@app.route('/<path:path>')
def send_static(path):
	return send_from_directory('www/', path)


if __name__ == '__main__':
	gv.init()
	app.run(debug=False, host="192.168.0.176", port="5000")
