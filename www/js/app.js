
angular
	.module("easytv-services", ["ngRoute"])
	.config(['$qProvider', function ($qProvider) {$qProvider.errorOnUnhandledRejections(false);}])
	.config(function($locationProvider, $routeProvider) {
		$locationProvider.html5Mode(true);
		$routeProvider
			.when("/", {
				templateUrl: '../views/login.html',
				controller:'login'

			})
			.when("/main", {
				templateUrl: '../views/main.html',
				controller:'main'

			})
			.when("/series/:service", {
				templateUrl: '../views/media.html',
				controller:'media'

			})
			.when("/anotation/:media", {
				templateUrl: '../views/anotation.html',
				controller:'anotation'

			})
			.when("/anotation/:media/:episode", {
				templateUrl: '../views/episode.html',
				controller:'episode'

			})
			.when("/textdetection/:media", {
				templateUrl: '../views/textdetection.html',
				controller:'textdetection'

			})
			.when("/verification/:media", {
				templateUrl: '../views/verification.html',
				controller:'verification'

			})
			.when("/verification/:media/:episode", {
				templateUrl: '../views/sounds.html',
				controller:'sounds'

			})
			.otherwise({ redirectTo: '/' });
	})
