
angular
	.module("easytv-services")
	.controller("login",['$scope','$http','Api',"$location",function($scope,$http,Api,$location){
		$scope.authenticate =function (){
			$(".auth-error").hide();
			Api.media($scope.apiKey).then(
				function(result){
					localStorage.setItem('x-api-key',$scope.apiKey);
					$location.path("/main")
				},
				function(error){
					$(".auth-error").show();
				
				}
			);
		}

		$scope.apiKey = localStorage.getItem('x-api-key')|| "";
		if ($scope.apiKey!=""){
			$scope.authenticate();
		
		}
		

	}])
	.controller("main",['$scope','$http','Api','$location',function($scope,$http,Api,$location){
		$scope.apiKey = localStorage.getItem('x-api-key');
		Api.media($scope.apiKey).then(
			function(result){

			},
			function(error){
				$location.path("/")
			
			}
		);
		$scope.logout =function (){
			localStorage.removeItem("x-api-key");			
			$location.path("/")
				
		}
		

	}])
	.controller("verification",['$scope','$http','Api','$routeParams','$location',function($scope,$http,Api,$routeParams,$location){
		$scope.apiKey = localStorage.getItem('x-api-key');
		Api.media($scope.apiKey).then(
			function(result){
				getEpisodes();
			},
			function(error){
				$location.path("/")
			
			}
		);

		var activeUrl = null;
		$scope.paused = true;
		$scope.media=$routeParams.media;
		$scope.episodes=[]
		$scope.status=[]

		

		function getEpisodes(){
			Api.getEpisodes($scope.media,$scope.apiKey).then(
				function(result){
					$scope.episodes=result.data.episodes
					$("#album").show();
					$("#loader").hide();
				},
				function(error){
					$("#album").show();
					$("#loader").hide();
					console.log(error)
				}


			);
		}

		$scope.normalizeName=function (name){
			return name.split("_").join(" ")
		}
		$scope.logout =function (){
			localStorage.removeItem("x-api-key");
			$location.path("/")
				
		}
	}])
	.controller("anotation",['$scope','$http','Api','$routeParams','$location',function($scope,$http,Api,$routeParams,$location){
		$scope.apiKey = localStorage.getItem('x-api-key');
		Api.media($scope.apiKey).then(
			function(result){
				getEpisodes();
			},
			function(error){
				$location.path("/")
			
			}
		);

		var activeUrl = null;
		$scope.paused = true;
		$scope.media=$routeParams.media;
		$scope.episodes=[]
		$scope.status=[]

		

		function getEpisodes(){
			Api.getEpisodes($scope.media,$scope.apiKey).then(
				function(result){
					$scope.episodes=result.data.episodes
					$scope.status=result.data.faces_status

					$("#album").show();
					$("#loader").hide();
				},
				function(error){
					$("#album").show();
					$("#loader").hide();
					console.log(error)
				}


			);
		}

		$scope.normalizeName=function (name){
			return name.split("_").join(" ")
		}
		$scope.logout =function (){
			localStorage.removeItem("x-api-key");
			$location.path("/")
				
		}
	}])
	.controller("sounds",['$scope','$http','Api','$routeParams','$location',function($scope,$http,Api,$routeParams,$location){
		$scope.apiKey = localStorage.getItem('x-api-key');
		var activeUrl = null;
		$scope.paused = true;
		$scope.media=$routeParams.media;
		$scope.events=new Array();
		$scope.videoSounds=new Array();
		$scope.sceneSound=""
		$scope.region=0
		$scope.episode=$routeParams.episode;
		$scope.status=[]
		regionToRemove=null;
		Api.media($scope.apiKey).then(
			function(result){
				load();
			},
			function(error){
				$location.path("/")
			
			}
		);

		

		

		function load() {
			$("#loader").show();
			$("#album").hide();
			var video=	document.getElementById("video")
			path="/media/"+$scope.media+"/"+$scope.episode+"/"

			video.src = path+$scope.episode+".mp4";

			video.autoplay = false;
			Api.getFaces(path+$scope.episode+'_sound_events.json',$scope.apiKey).then(
				function(data) {
					$scope.regions=createRegions(data.data);
					$scope.events= getEvents();
					$scope.wavesurfer = createWavesurfer();

					url=path+$scope.episode+".mp3";
					activeUrl = url;
					$scope.wavesurfer.once('ready', function() {
						$("#loader").hide();
						$("#album").hide();
						$("#anotation-tool").show();

					});
					$scope.wavesurfer.load(activeUrl);

				},
				function (error){
					Api.getFaces(path+'env_sounds.json',$scope.apiKey).then(
						function(data) {
							$scope.regions=createRegions(data.data);
							$scope.events= getEvents();
							$scope.wavesurfer = createWavesurfer();

							url=path+$scope.episode + "_foreground.wav";
							activeUrl = url;
							$scope.wavesurfer.once('ready', function() {
								$("#loader").hide();
								$("#album").hide();
								$("#anotation-tool").show();

							});
							$scope.wavesurfer.load(activeUrl);

						},
						function (error){
							$("#loader").hide();
							$("#preprocessing").show();
						}
					);
				}
			);

		};

		function createRegions(data){
			regions=[];
			for(i=0;i<data.Output.length;i++){
				regions.push({start:data.Output[i].start,end:data.Output[i].end,color:randomColor(0.2),event:data.Output[i].event})

			}
			return regions
		}

		$scope.isPlaying = function(url) {
			return url == activeUrl;
		};
		$scope.addEvents = function(event) {
			$scope.events.push(event);
		};
		function createWavesurfer(){
			var wavesurfer = WaveSurfer.create({
				container: "#waveform",
				scrollParent: true,
				waveColor:"#337ab7",
				progressColor:"#23527c",
				height:"150", 
				plugins: [
						WaveSurfer.regions.create({
							regions:$scope.regions,
							dragSelection: {slop: 5}
						}),
						WaveSurfer.timeline.create({
							container: "#wave-timeline"
						}),
						WaveSurfer.minimap.create({
							height: 30,
							waveColor: '#ddd',
							progressColor: '#999',
							cursorColor: '#999'
						})
					]
				})
			wavesurfer.setMute(true); 
			wavesurfer.on('play', function() {
				var video = document.getElementById("video");
				video.currentTime=Math.round(wavesurfer.getCurrentTime() * 10) / 10;
				video.play();
				$scope.paused = false;

				try{
					$scope.$apply();
				}catch(e){
					//console.log(e)
				}


			});

			wavesurfer.on('pause', function() {
				var video = document.getElementById("video");
				video.pause(); 
				$scope.paused = true;
				try{
					$scope.$apply();
				}catch(e){
					//console.log(e)
				}
				
			});
			wavesurfer.on('error', function(err){
				//TODO Mostrar prepocessing
				$("#loader").hide();
				$("prepocessing").show();
			})
			wavesurfer.on('finish', function() {
				$scope.paused = true;
				$scope.wavesurfer.seekTo(0);
				$scope.$apply();
			});
		    wavesurfer.on('region-click', function(region, e) {
				e.stopPropagation();
				var video=	document.getElementById("video");
				keys = Object.keys($scope.wavesurfer.regions.list)
				$scope.region=keys.indexOf(region.id);
				try{
					$scope.sceneSound=$scope.regions[$scope.region].event
				}catch(e){
					$scope.regions[$scope.region].event=""
					$scope.sceneSound=""
				}
				// Play on click, loop on shift click
				region.play();


			});

			wavesurfer.on('region-update-end', function(region) {
				keys = Object.keys($scope.wavesurfer.regions.list)
				$scope.region=keys.indexOf(region.id);
				$scope.regions[$scope.region].start=region.start;
				$scope.regions[$scope.region].end=region.end;
				console.log($scope.regions[$scope.region]);
				save();

			});
			
			
			 wavesurfer.on('region-dblclick', function(region, e) {
			 	$('#modalCenter').modal('toggle');
			 	$scope.wavesurfer.pause();
				regionToRemove=region;
				keys = Object.keys($scope.wavesurfer.regions.list)
				$scope.region=keys.indexOf(region.id);
				


			});
			 wavesurfer.on('region-created', function(region, e) {
			 	console.log(region);
			 	region.color=randomColor(0.2);
				$scope.regions.push({start:region.start,end:region.end,color:region.color,event:""})
				save();
				
			});
			
			 wavesurfer.on('seek', function(status) {
			 	if (status==1){
			 		if(!$scope.paused){
			 			var video = document.getElementById("video");
						video.currentTime=Math.round(wavesurfer.getCurrentTime() * 10) / 10;
						video.play();
			 		}
			 	}


			});
				 

			return wavesurfer;
		}
	
		function randomColor(alpha) {
		    return (
		        'rgba(' +
		        [
		            ~~(Math.random() * 255),
		            ~~(Math.random() * 255),
		            ~~(Math.random() * 255),
		            alpha || 1
		        ] +
		        ')'
		    );
		}
		$scope.removeRegion= function (){
			$('#modalCenter').modal('toggle');
			regionToRemove.remove();
			$scope.regions.splice($scope.region, 1);
			regionToRemove=null;
			$scope.events= getEvents();
		}
		$scope.play= function (){
			if($scope.paused){
				$scope.wavesurfer.play();
			}else{
				$scope.wavesurfer.pause();
			}
		}
		$scope.handleDragStart = function(e){
			this.style.opacity = '0.4';
			e.dataTransfer.setData('text/plain', this.innerHTML);
		};

		$scope.handleDragEnd = function(e){
			this.style.opacity = '1.0';
		};
    
		$scope.handleDrop = function(e){
			e.preventDefault();
			e.stopPropagation();
			var dataText = strip_html_tags(e.dataTransfer.getData('text/plain'));
			$scope.sceneSound=dataText;
			$scope.regions[$scope.region].event=dataText;
			$scope.$apply();
			save();
			
		};
		$scope.remove = function(){
			$scope.regions[$scope.region].event=null;
			$scope.sceneSound=null;
			save()
		}
		$scope.removeSound = function(name){
			for(i=0;i<$scope.regions.length;i++){
				if($scope.regions[i].event==name){

					$scope.regions[i].event=null
				}
			}
			if($scope.sceneSound==name){
				$scope.sceneSound=null;
			}
			$scope.events= getEvents();
			save()
		}
		function getEvents(){
			events=[];
			for(i=0;i<$scope.regions.length;i++){
				if (!events.includes($scope.regions[i].event)){
					events.push($scope.regions[i].event)
				}
			}
			return events
		}
		$scope.handleDragOver = function (e) {
			e.preventDefault(); // Necessary. Allows us to drop.
			e.dataTransfer.dropEffect = 'move';  // See the section on the DataTransfer object.
			return false;
		};
		$scope.back = function(){
			console.log()
			$location.path("/verification/"+$scope.media)
		}
		function onlyUnique(value, index, self) { 
			return self.indexOf(value) === index && value!=null
		}
		function strip_html_tags(str)
		{
			if ((str===null) || (str===''))
				return null;
			else
				str = str.toString();
				str= str.replace(/<[^>]*>/g, '');
			return str.substr(0, str.length-1).trim();
		}

		function save(){
			Api.saveSounds({"events":$scope.regions},$scope.media,$scope.episode,$scope.apiKey).then(
				function(result){
					//console.log("result");
				},
				function(error){
					console.log(error)
				}

			);
		}

		$scope.normalizeName=function (name){
			return name.split("_").join(" ")
		}
		$scope.logout =function (){
			localStorage.removeItem("x-api-key");
			$location.path("/")
				
		}
	}])
	.controller("episode",['$scope','$http','Api','$routeParams','$location',"$window",function($scope,$http,Api,$routeParams,$location,$window){
		$scope.apiKey = localStorage.getItem('x-api-key');
		var activeUrl = null;
		$scope.paused = true;
		$scope.media=$routeParams.media;
		$scope.characters=new Array();
		$scope.videoCharacters=new Array();
		$scope.sceneCharacter=""
		$scope.region=0
		$scope.episode=$routeParams.episode;
		$scope.status=[];
		$scope.boundaires=[];
		angular.element($window).bind('resize', function(){
			resize_canvas();
		});
		Api.media($scope.apiKey).then(
			function(result){
				load();
			},
			function(error){
				$location.path("/")
			
			}
		);

		

		

		function load() {
			$("#loader").show();
			$("#album").hide();
			var video=	document.getElementById("video")
			path="/media/"+$scope.media+"/"+$scope.episode+"/"

			video.src = path+$scope.episode+".mp4";
			video.autoplay = false;
			Api.getFaces(path+$scope.episode+'_faces_annotator.json',$scope.apiKey).then(
				function(data) {
					regions=createRegions(data.data);
					
					
					$scope.wavesurfer = createWavesurfer(regions);

					url=path+$scope.episode+".mp3";
					activeUrl = url;
					$scope.wavesurfer.once('ready', function() {
						
						$("#loader").hide();
						$("#album").hide();
						$("#anotation-tool").show();
						$scope.$apply();
						resize_canvas();

					});
					$("#video").html($scope.video);

					Api.getCharacters($scope.media,$scope.episode,$scope.apiKey).then(
						function(result){
							$scope.videoCharacters=result.data
							$scope.characters= $scope.videoCharacters.filter( onlyUnique );
						},
						function(error){
							$scope.videoCharacters=new Array(data.data.frames.length)
						}

					);
					$scope.wavesurfer.load(activeUrl);

				},
				function (error){
					$("#loader").hide();
					$("#preprocessing").show();
				}
			);
		};

		function createRegions(frames){
			regions=[];
			for(i=0;i<frames.frames.length;i++){
				regions.push({start:frames.frames[i][0]/frames.fps,end:frames.frames[i][1]/frames.fps,color:randomColor(0.2)})
				$scope.boundaires.push({x:frames.frames[i][2],y:frames.frames[i][3],w:frames.frames[i][4],h:frames.frames[i][5]});
			}
			return regions
		}

		$scope.isPlaying = function(url) {
			return url == activeUrl;
		};
		$scope.addCharacters = function(character) {
			$scope.characters.push(character);
		};
		function normalize(data){
			var video = document.getElementById('video');
			result={};
			var w = video.offsetWidth;
			var h = video.offsetHeight;
			result.x=data.x*w/1280;
			result.y=data.y*h/720;
			result.w=data.w*w/1280;
			result.h=data.h*h/720;
			return result
		}
		function createWavesurfer(regions){
			var wavesurfer = WaveSurfer.create({
				container: "#waveform",
				scrollParent: true,
				waveColor:"#337ab7",
				progressColor:"#23527c",
				height:"150", 
				plugins: [
						WaveSurfer.regions.create({
							regions:regions
							}),
						WaveSurfer.timeline.create({
							container: "#wave-timeline"
						}),
						WaveSurfer.minimap.create({
							height: 30,
							waveColor: '#ddd',
							progressColor: '#999',
							cursorColor: '#999'
						})
					]
				})
				wavesurfer.setMute(true); 
				wavesurfer.on('play', function() {
					$scope.paused = false;
				});

				wavesurfer.on('pause', function() {
					var video = document.getElementById("video");
					video.pause(); 
					
				});
				wavesurfer.on('error', function(err){
					//TODO Mostrar prepocessing
					$("#loader").hide();
					$("prepocessing").show();
				})
				wavesurfer.on('finish', function() {
					$scope.paused = true;
					$scope.wavesurfer.seekTo(0);
					$scope.$apply();
				});
			    wavesurfer.on('region-click', function(region, e) {
					e.stopPropagation();

					var video=	document.getElementById("video");
					keys = Object.keys($scope.wavesurfer.regions.list)
					$scope.region=keys.indexOf(region.id);
					try{
						$scope.sceneCharacter=$scope.videoCharacters[$scope.region];
											paintRect($scope.boundaires[$scope.region]);

					}catch(e){
						$scope.videoCharacters[$scope.region]=""
						$scope.sceneCharacter=""
					}
					// Play on click, loop on shift click
					region.play();
					var video = document.getElementById("video");
					video.currentTime=Math.round(region.start * 10) / 10;
					video.play();
					
					$scope.$apply();

				});
				

			return wavesurfer;
		}
		function paintRect(data){
			var canvas = document.getElementById('canvas');
			var ctx = canvas.getContext('2d');
			ctx.clearRect(0,0,canvas.width,canvas.height); //clear canvas
			ctx.beginPath();
			var width =data.size;
			var height = data.size;
			result=normalize(data)
			ctx.rect(result.x,result.y,result.w,result.h);
			ctx.strokeStyle = "#ffc107";
			ctx.lineWidth = 10;
			ctx.stroke();
		}
		function resize_canvas()
		{
			var element=	document.getElementById("video")
			var w = element.offsetWidth;
			var h = element.offsetHeight;
			var cv = document.getElementById("canvas");
			cv.width = w;
			cv.height =h;
		}
		function randomColor(alpha) {
		    return (
		        'rgba(' +
		        [
		            ~~(Math.random() * 255),
		            ~~(Math.random() * 255),
		            ~~(Math.random() * 255),
		            alpha || 1
		        ] +
		        ')'
		    );
		}
		$scope.handleDragStart = function(e){
			this.style.opacity = '0.4';
			e.dataTransfer.setData('text/plain', this.innerHTML);
		};

		$scope.handleDragEnd = function(e){
			this.style.opacity = '1.0';
		};
    
		$scope.handleDrop = function(e){
			e.preventDefault();
			e.stopPropagation();
			var dataText = strip_html_tags(e.dataTransfer.getData('text/plain'));
			$scope.sceneCharacter=dataText;
			$scope.videoCharacters[$scope.region]=dataText;
			$scope.$apply();
			save();
			
		};

		$scope.remove = function(){
			$scope.videoCharacters[$scope.region]=null;
			$scope.sceneCharacter=null;
			save()
		}
		$scope.removeCharacter = function(name){
			for(i=0;i<$scope.videoCharacters.length;i++){
				if($scope.videoCharacters[i]==name){

					$scope.videoCharacters[i]=null
				}
			}
			if($scope.sceneCharacter==name){
				$scope.sceneCharacter=null;
			}
			$scope.characters= $scope.videoCharacters.filter( onlyUnique );
			save()
		}
		$scope.handleDragOver = function (e) {
			e.preventDefault(); // Necessary. Allows us to drop.
			e.dataTransfer.dropEffect = 'move';  // See the section on the DataTransfer object.
			return false;
		};
		$scope.back = function(){
			console.log()
			$location.path("/anotation/"+$scope.media)
		}
		function onlyUnique(value, index, self) { 
			return self.indexOf(value) === index && value!=null
		}
		function strip_html_tags(str)
		{
			if ((str===null) || (str===''))
				return null;
			else
				str = str.toString();
				str= str.replace(/<[^>]*>/g, '');
			return str.substr(0, str.length-1).trim();
		}

		function save(){
			Api.saveCharacters({"characters":$scope.videoCharacters},$scope.media,$scope.episode,$scope.apiKey).then(
				function(result){
					//console.log("result");
				},
				function(error){
					console.log(error)
				}

			);
		}

		$scope.normalizeName=function (name){
			return name.split("_").join(" ")
		}
		$scope.logout =function (){
			localStorage.removeItem("x-api-key");
			$location.path("/")
				
		}
	}])
	.controller("textdetection",['$scope','$http','$window','Api','$routeParams','$location',function($scope,$http,$window,Api,$routeParams,$location){
		$scope.apiKey = localStorage.getItem('x-api-key');
		Api.media($scope.apiKey).then(
			function(result){
				getEpisodes()
			},
			function(error){
				$location.path("/")
			
			}
		);
		$scope.areas=[];
		$scope.colors=["#ffc107","#dc3545","#28a745","#007bff"];
		$scope.bg=["warning","danger","success","primary"];
		$scope.episode=""
		$scope.playing=true
		var last_mousex = last_mousey = 0;
		var mousex = mousey = 0;
		var mousedown = false;
		var video=	document.getElementById("video")


		var canvas = document.getElementById('canvas');
		var ctx = canvas.getContext('2d');
		$scope.media=$routeParams.media
		angular.element($window).bind('resize', function(){
			resize_canvas();
			paintAllAreas()
		});
		video.onplay=function(){resize_canvas();paintAllAreas();}
		video.addEventListener('error', function(event) {
			$("#loader").hide();
			$("#textdetection-tool").hide();
			$("#album").hide();
			$("#preprocessing").show();
		}, true);


		//Mousedown
		$(canvas).on('mousedown', function(e) {
			canvasx = $(canvas).offset().left;
			canvasy = $(canvas).offset().top;
			last_mousex = parseInt(e.clientX-canvasx);
			last_mousey = parseInt(e.clientY-canvasy);
			mousedown = true;
		});

		//Mouseup
		$(canvas).on('mouseup', function(e) {
			canvasx = $(canvas).offset().left;
			canvasy = $(canvas).offset().top;
			mousex = parseInt(e.clientX-canvasx);
			mousey = parseInt(e.clientY-canvasy);
			rect= normalize({x0:Math.min(mousex,last_mousex),y0:Math.min(mousey,last_mousey),x1:Math.max(mousex,last_mousex),y1:Math.max(mousey,last_mousey)})
			if($scope.areas.length<4){
				$scope.areas.push(rect)
			}else{
				$scope.areas[3]=rect
			}
			save($scope.areas)
			mousedown = false;

			paintAllAreas()
		});

		//Mousemove
		$(canvas).on('mousemove', function(e) {
			canvasx = $(canvas).offset().left;
			canvasy = $(canvas).offset().top;
			mousex = parseInt(e.clientX-canvasx);
			mousey = parseInt(e.clientY-canvasy);
			
				if(mousedown){
					ctx.clearRect(0,0,canvas.width,canvas.height); //clear canvas
					ctx.beginPath();
					var width = mousex-last_mousex;
					var height = mousey-last_mousey;
					ctx.rect(last_mousex,last_mousey,width,height);
					color=$scope.areas.length==4?4:$scope.areas.length;
					ctx.strokeStyle = $scope.colors[$scope.areas.length];
					ctx.lineWidth = 10;
					ctx.stroke();
				
				}
		})
		$scope.loadCanvas=function (episode){
			
			$scope.episode=episode
			var video=	document.getElementById("video")
			path=	"/media/"+$scope.media+"/"+episode+"/"
			video.src = path+episode+".mp4";
			video.autoplay = true;
			Api.getTextDetectionArea($scope.media,episode,$scope.apiKey).then(
				function(result){
					$scope.areas=result.data
					$("#loader").hide();
					$("#album").hide();
					$("#textdetection-tool").show();
					resize_canvas();
					paintAllAreas();

					
				},
				function(error){
					console.log(error)
					$("#loader").hide();
					$("#album").hide();
					$("#textdetection-tool").show();
				}
			);
			
		}
		$scope.backward= function(){
			var video = document.getElementById('video');
			position=video.currentTime ;
			position-=10
			if(position<0)	position=0
			video.currentTime =position
			if($scope.playing){
				video.play();
			}
		}
		$scope.fordward= function(){
			var video = document.getElementById('video');
			position=video.currentTime ;
			position+=10
			if(position>=video.duration)	position=video.duration
			video.currentTime =position
			if($scope.playing){
				video.play();
			}
		}
		$scope.play= function(){
			var video = document.getElementById('video');
			if($scope.playing){
				video.pause()
				$scope.playing=false
			}else{
				video.play()
				$scope.playing=true
			}
		}
		function normalize(data){
			var video = document.getElementById('video');
			var w = video.offsetWidth;
			var h = video.offsetHeight;
			data.x1=data.x1/w;
			data.x0=data.x0/w;
			data.y1=data.y1/h;
			data.y0=data.y0/h;
			return data
		}
		function paintAllAreas(){
			var canvas = document.getElementById('canvas');
			var ctx = canvas.getContext('2d');
			var video = document.getElementById('video');
			var w = video.offsetWidth;
			var h = video.offsetHeight;
			ctx.clearRect(0,0,canvas.width,canvas.height); //clear canvas
			for (index in $scope.areas){
				if (index<4){

					area=$scope.areas[index];
				
					var width = area.x0*w-area.x1*w;
					if(width==0) width=20;
					var height = area.y0*h-area.y1*h;
					if(height==0) height=20;
					ctx.beginPath();
					ctx.rect(area.x1*w,area.y1*h,width,height);
					ctx.strokeStyle =$scope.colors[index];
					ctx.lineWidth = 10;
					ctx.stroke();

				}

			}
		}
		$scope.remove = function(index){
			$scope.areas.splice(index,1);
			paintAllAreas();
			save($scope.areas)

		}
		$scope.back = function(){
			$scope.episode="";
			var video=	document.getElementById("video")
			video.pause()
			$("#loader").show();
			$("#album").hide();
			$("#textdetection-tool").hide();
			$("#preprocessing").hide();
			$scope.areas=[]
			var ctx = canvas.getContext('2d');
			ctx.clearRect(0,0,canvas.width,canvas.height); //clear canvas

			getEpisodes()
		}
		function getEpisodes(){
			Api.getEpisodes($scope.media,$scope.apiKey).then(
				function(result){
					$scope.episodes=result.data.episodes
					$("#album").show();
					$("#loader").hide();
				},
				function(error){
					$("#album").show();
					$("#loader").hide();
					console.log(error)
				}


			);
		}
		function resize_canvas()
		{
			var element=	document.getElementById("video")
			var w = element.offsetWidth;
			var h = element.offsetHeight;
			var cv = document.getElementById("canvas");
			cv.width = w;
			cv.height =h;
		}
		function save(data){
			Api.saveTextDetectionArea({"area":data},$scope.media,$scope.episode,$scope.apiKey).then(
				function(result){
					//console.log(result);
				},
				function(error){
					console.log(error)
				}

			);
		}
		$scope.normalizeName=function (name){
			return name.split("_").join(" ")
		}
		$scope.logout =function (){
			localStorage.removeItem("x-api-key");
			$location.path("/")
				
		}
	}])
	.controller("media",['$scope','$http','Api', '$routeParams','$location',function($scope,$http,Api, $routeParams,$location){

		var activeUrl = null;
		$scope.paused = true;
		$scope.media=[];
		$scope.service= $routeParams.service;
		$scope.apiKey = localStorage.getItem('x-api-key');
		getMedia()
		function getMedia(){
			Api.media($scope.apiKey).then(
				function(result){
					$scope.media=result.data.media
					$("#album").show();
					$("#loader").hide();
				},
				function(error){
					$("#album").show();
					$("#loader").hide();
					console.log(error)
				}
			);
		}
		$scope.normalizeName=function (name){
			return name.split("_").join(" ")
		}
		$scope.logout =function (){
			localStorage.removeItem("x-api-key");
			$location.path("/")
				
		}
		
	}])
