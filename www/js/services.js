

angular
	.module("easytv-services")
	.factory("Api" ,function($http){
		var factory = {};
		var apiKey =  null;

		PATH="http://138.4.47.33:8080/"
		console.log(PATH)
		factory.media = function(apiKey) {
			url=PATH+"media";
			return $http.get(url, {headers: {'x-api-key':apiKey }});
		}
		factory.getEpisodes = function(media,apiKey) {
			url=PATH+"media/"+media;
			return $http.get(url, {headers: {'x-api-key':apiKey }});
		}
		factory.saveCharacters = function(data,media,episode,apiKey) {
			console.log(data);
			url=PATH+"characters/"+media+"/"+episode;
			return $http.post(url,data, {headers: {'x-api-key':apiKey }});
			
		}
		factory.saveTextDetectionArea = function(data,media,episode,apiKey) {
			url=PATH+"textdetection/area/"+media+"/"+episode;
			return $http.post(url,data, {headers: {'x-api-key':apiKey }});
			
		}
		factory.getTextDetectionArea = function(media,episode,apiKey) {

			url=PATH+"textdetection/area/"+media+"/"+episode;
			return $http.get(url, {headers: {'x-api-key':apiKey }});
			
		}
		factory.getCharacters = function(media,episode,apiKey) {
			url=PATH+"characters/"+media+"/"+episode;
			return $http.get(url, {headers: {'x-api-key':apiKey }});
			
		}
		factory.saveSounds = function(data,media,episode,apiKey) {
			url=PATH+"sounds/"+media+"/"+episode;
			return $http.post(url,data, {headers: {'x-api-key':apiKey }});
			
		}
			
	
		factory.getFaces = function(url,apiKey) {
				return $http.get(url, {headers: {'x-api-key':apiKey }});
				
			}
		return factory;

	})
