angular
	.module("easytv-services")
	.directive('ngWavesurfer', function() {
		return {
			restrict: 'E',
			link: function($scope, $element, $attrs) {
				$element.css('display', 'block');
				frames=JSON.parse($attrs.charactersData);
				regions=[];
				colors=["darkblue","darkorange","green","darkred","darkviolet","#B3009999","#B3cc0066","#B366ff66","#B3cc0000","#B3990099"]
				for(i=0;i<frames.frames.length;i++){
					regions.push({start:frames.frames[i][0]/frames.fps,end:frames.frames[i][1]/frames.fps,color:colors[i%10]})
				}
				console.log(regions)
				var options = angular.extend({ container: $element[0] ,scrollParent: true,
						plugins: [
							WaveSurfer.regions.create({
								regions:regions,
								dragSelection: {
									slop: 5
								}
							}),
								WaveSurfer.minimap.create({
								height: 30,
								waveColor: '#ddd',
								progressColor: '#999',
								cursorColor: '#999'
							}),
							WaveSurfer.timeline.create({
								container: "#wave-timeline"
							})
						]},
					$attrs);
				var wavesurfer = WaveSurfer.create(options);
				if ($attrs.url) {
					wavesurfer.load($attrs.url, $attrs.data || null);
				}
				$scope.$emit('wavesurferInit', wavesurfer);
			}
		};
	})
	.directive('draggable', function () {
		return {
			restrict: 'A',
			link: function (scope, element, attrs) {
				element[0].addEventListener('dragstart', scope.handleDragStart, false);
				element[0].addEventListener('dragend', scope.handleDragEnd, false);
			}
		}
	})
	.directive('droppable', function () {
		return {
			restrict: 'A',
			link: function (scope, element, attrs) {
				element[0].addEventListener('drop', scope.handleDrop, false);
				element[0].addEventListener('dragover', scope.handleDragOver, false);
			}
		}
	})




