# filter warnings
import warnings
warnings.simplefilter(action="ignore", category=FutureWarning)

import tensorflow as tf

# keras imports
from keras.applications.vgg16 import VGG16, preprocess_input
from keras.applications.vgg19 import VGG19, preprocess_input
from keras.applications.xception import Xception, preprocess_input
from keras.applications.resnet50 import ResNet50, preprocess_input
from keras.applications.inception_resnet_v2 import InceptionResNetV2, preprocess_input
from keras.applications.mobilenet import MobileNet, preprocess_input
from keras.applications.inception_v3 import InceptionV3, preprocess_input
from keras.preprocessing import image
from keras.models import Model
from keras.models import model_from_json
from keras.layers import Input
from keras.preprocessing.image import ImageDataGenerator
from keras.layers import GlobalAveragePooling2D, Dense, Dropout
from keras.callbacks import TensorBoard,ModelCheckpoint
from keras import optimizers 
from keras import models
from keras import layers
from keras import optimizers

# other imports
from sklearn.preprocessing import LabelEncoder
import numpy as np
import glob
import cv2
import h5py
import os
import json
import datetime
import time

graph = tf.get_default_graph()

def face_training(video_path=None):

	global graph
	with graph.as_default():


		folders = 0
		for _, dirnames, filenames in os.walk('www/media/Com_si_fos_ahir/Dataset/images'):
			folders += len(dirnames)

		# config variables
		include_top   = False
		batch_size = 64

		#image_input = Input(shape=(224, 224, 3))
		#base_model = ResNet50(input_tensor=image_input, include_top=True, weights='imagenet')
		vgg_conv = ResNet50(weights='imagenet', include_top=False, input_shape=(224, 224, 3))
		#for layer in vgg_conv.layers[:-4]:
		#	layer.trainable = False

		# Create the model
		model = models.Sequential()
		# Add the vgg convolutional base model
		model.add(vgg_conv)
		# Add new layers
		model.add(layers.Flatten())
		model.add(layers.Dense(1024, activation='relu'))
		model.add(layers.Dropout(0.5))
		model.add(layers.Dense(1024, activation='relu'))
		model.add(layers.Dropout(0.5))
		model.add(layers.Dense(folders, activation='softmax'))
		 
		# Show a summary of the model. Check the number of trainable parameters
		model.summary()


		'''x=base_model.output
		x=Dense(1024,activation='relu')(x) #we add dense layers so that the model can learn more complex functions and classify for better results.
		preds=Dense(folders,activation='softmax')(x) #final layer with softmax activation
		model=Model(inputs=base_model.input,outputs=preds)
		model.summary()'''

		print("[INFO] successfully loaded base model and model...")

		datagen = ImageDataGenerator(validation_split=0.25, rescale=1./255, shear_range=0.2, zoom_range=0.2, horizontal_flip=True, width_shift_range=0.2, height_shift_range=0.2, fill_mode='nearest')
		TRAIN_DIR = 'www/media/Com_si_fos_ahir/Dataset/images'

		train_generator = datagen.flow_from_directory(TRAIN_DIR, subset='training', batch_size=batch_size, target_size=(224, 224))
		val_generator = datagen.flow_from_directory(TRAIN_DIR, subset='validation', batch_size=batch_size, target_size=(224, 224))

		filepath = 'ResNet50_model_weights.h5'
		#checkpoint = ModelCheckpoint(filepath, monitor='val_loss', verbose=1, save_best_only=True,save_weights_only=False, mode='min', period=1)
		#callbacks_list = [checkpoint]

		# Compile the model
		model.compile(loss='categorical_crossentropy',
		              optimizer=optimizers.Adam(lr=1e-3),
		              metrics=['acc'])
		# Train the model
		history = model.fit_generator(
		      train_generator,
		      steps_per_epoch=train_generator.samples/train_generator.batch_size ,
		      epochs=10,
		      validation_data=val_generator,
		      validation_steps=val_generator.samples/val_generator.batch_size,
		      verbose=1)
		 
		# Save the model
		model.save(filepath)


if __name__ == '__main__':
	face_training()