from text_detection.text_extraction import text_extraction
from text_detection.text_process import text_process
from text_detection.text_presentation import text_presentation
from text_detection.text_subs import text_subs
from mpd_service.mpd_editor import mpd_editor

#import threading
from helper import global_variables as gv
import os
from flask import abort

import multiprocessing

def start(media="",episode=""):
	
	try:
		if not gv.proccesing.value:
			response={}
			init_path="www/media/"+media+"/"
			area_path = "www/media/"+media+"/"+episode+"/"+episode+"_text_settings.json"
			if not media in gv.text_status.keys():
				gv.text_status[media]={}

			#if os.path.isfile(area_path):
			# Procesar el video con el modelo

			aux=gv.text_status[media]
			aux[episode]=gv.Status.PREPROCESSING.value
			gv.text_status[media]=aux
			#thread_extraction = threading.Thread(target=data_extraction, args=(media, episode, init_path+episode+"/"+episode+".mp4"))
			#thread_extraction.start()

			p = multiprocessing.Process(target=data_extraction, args=(media, episode, init_path+episode+"/"+episode+".mp4"))
			p.start()

			response={"message":"ok","status":"200"}
			'''	
			else:
				#Video not anotated
				aux=gv.video_status[media]
				aux[episode]=gv.Status.WAITINGUSERANOTATION.value
				gv.video_status[media]=aux
				response={"message":"Please anotate the video","status":"200"}
			'''

		else:
			response={"message":"Server is already preprocessing another video","status":"200"}

	except Exception as e:
		gv.logger.error(e)
		abort(500)
	return response

def data_extraction(media,episode,video_path):
	gv.proccesing=multiprocessing.Value('i', 1)
	text_extraction(video_path=video_path)
	text_process(video_path=video_path)
	text_subs(video_path=video_path)
	mpd_editor(video_path=video_path, service="text_detection")
	if gv.text_status[media][episode]==gv.Status.GENERATINGANOTATIONFILE.value:
		aux=gv.text_status[media]
		aux[episode]=gv.Status.WAITINGUSERANOTATION.value
		gv.text_status[media]=aux
	elif gv.text_status[media][episode]==gv.Status.PREPROCESSING:
		aux=gv.text_status[media]
		aux[episode]=gv.Status.PROCESSED.value
		gv.text_status[media]=aux		

	gv.proccesing=multiprocessing.Value('i', 0)


