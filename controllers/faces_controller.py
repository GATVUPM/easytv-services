from faces_service.face_detection import face_detection
from faces_service.face_process import face_process
from faces_service.face_process_v3 import face_process_final
from annotator_service.face_training import face_training
from annotator_service.face_characters import face_characters
from annotator_service.face_extraction import face_extraction
from mpd_service.mpd_editor import mpd_editor
from fusion_process.create_final_json import fusion_process

#from annotator_service.face_training_v2 import face_training

#import threading
from helper import global_variables as gv
import os
from flask import abort

import multiprocessing

def start(media="",episode=""):
	
	try:
		manager = multiprocessing.Manager()

		if not gv.proccesing.value:
			response={}
			init_path="www/media/"+media+"/"
			model_name = init_path+"ResNet50_model_weights.h5"
			if not media in gv.video_status.keys():
				gv.video_status[media]={}

			if os.path.isfile(model_name):
				# Procesar el video con el modelo
				gv.video_status[media][episode]=gv.Status.PROCESSING.value
				#thread_extraction = threading.Thread(target=data_extraction, args=(media,episode,init_path+episode+"/"+episode+".mp4"))
				#thread_extraction.start()
				
				p = multiprocessing.Process(target=data_extraction, args=(media,episode,init_path+episode+"/"+episode+".mp4"))
				p.start()

				response={"message":"ok","status":"200"}
				
			else:

				#p = multiprocessing.Process(target=data_extraction, args=(media,episode,init_path+episode+"/"+episode+".mp4"))
				#p.start()
				
				anotation_file=init_path+episode+"/"+episode+"_faces_annotator.json"
				if os.path.isfile(anotation_file):
					characters_file=init_path+episode+"/"+episode+"_characters_annotations.json"
					if os.path.isfile(characters_file):
						#No enough anotated videos to create the model
						gv.video_status[media][episode]=gv.Status.ANNOTATED.value
						response={"message":"This video is anotated, but the model is not trained","status":"200"}
					else:
						#Video not anotated
						gv.video_status[media][episode]=gv.Status.WAITINGUSERANOTATION.value
						response={"message":"Please anotate the video","status":"200"}
				else:
					#Create anotation file
					aux=gv.video_status[media]
					aux[episode]=gv.Status.GENERATINGANOTATIONFILE.value
					gv.video_status[media]=aux
					#thread_extraction = threading.Thread(target=data_extraction, args=(media,episode,init_path+episode+"/"+episode+".mp4"))
					#thread_extraction.start()
					p = multiprocessing.Process(target=data_extraction, args=(media,episode,init_path+episode+"/"+episode+".mp4"))
					p.start()

					response={"message":"Generating the anotation file","status":"200"}

		else:

			response={"message":"Server is already preprocessing another video","status":"200"}
	except Exception as e:
		gv.logger.error(e)
		abort(500)
	return response



def data_extraction(media,episode,video_path):
	gv.proccesing=multiprocessing.Value('i', 1)
	face_detection(video_path=video_path)
	face_process(video_path=video_path)
	face_process_final(video_path=video_path)
	mpd_editor(video_path=video_path, service="face_detection")
	if gv.video_status[media][episode]==gv.Status.GENERATINGANOTATIONFILE.value:
		aux=gv.video_status[media]
		aux[episode]=gv.Status.WAITINGUSERANOTATION.value
		gv.video_status[media]=aux
	elif gv.video_status[media][episode]==gv.Status.PROCESSING.value:
		aux=gv.video_status[media]
		aux[episode]=gv.Status.PROCESSED.value
		gv.video_status[media]=aux
	gv.proccesing=multiprocessing.Value('i', 0)
	#fusion_process(video_path=video_path)


def train_thread(media,files_clean,files):
	gv.proccesing=multiprocessing.Value('i',1)
	
	for file in files_clean:
		face_extraction(video_path=os.path.join("www","media",media,file,file+".mp4"))
	
	print("Train")
	face_training(video_path=os.path.join("www","media",media,files[0],files[0]+".mp4"))
	
	for file in files_clean:
		face_characters(video_path=os.path.join("www","media",media,file,file+".mp4"))
		mpd_editor(video_path=os.path.join("www","media",media,file,file+".mp4"), service="face_detection")
	gv.proccesing=multiprocessing.Value('i', 0)	


def train(media):
	response={}
	files=os.listdir(os.path.join("www","media",media))
	files=[file for file in files if os.path.isdir(os.path.join("www","media",media,file))]
	if "Dataset" in files:
		files = [item for item in files if item != "Dataset"]
	files_clean=[]
	for file in files:
		if os.path.isfile(os.path.join("www","media",media,file,file+"_characters_annotations.json")):
			files_clean.append(file)
	if len(files_clean)>=1:
		#thread_extraction = threading.Thread(target=train_thread, args=(media,files_clean,files))
		#thread_extraction.start()
		
		p = multiprocessing.Process(target=train_thread, args=(media,files_clean,files))
		p.start()

		response={"message":"Training model for"+media,"status":"200"}

	else:
		response={"message":"Please anotate more videos","status":"200"}

	return response
		
