import os
import multiprocessing
from helper import global_variables as gv
from sound_service import services
from flask import abort

serv = services.MediaSoundAnalysisService()

# ==================================================================================
# -------------------------------- Generate Dataset --------------------------------
# ==================================================================================

def create_speaker_dataset_thread(media):
    # Generate New Service
    if serv.audio_analysis_manager is None:
        serv.init_manager()
    # Modify Processing status
    gv.proccesing = multiprocessing.Value('i', 1)
    serv.generate_new_dataset(media)
    gv.proccesing = multiprocessing.Value('i', 0)


def create_speaker_dataset(media):
    response = {}
    if not gv.proccesing.value:
        # Start Thread
        p = multiprocessing.Process(target=create_speaker_dataset_thread, args=(media,))
        p.start()
        response = {"message": "Generating Speaker Dataset for " + media, "status": "200"}
    else:
        response = {"message": "Server is busy generating the speaker dataset", "status": "200"}
    return response


# ==================================================================================
# ----------------------------------- Train Model ----------------------------------
# ==================================================================================


def train_speaker_model_thread(media=""):
    # Generate New Service
    if serv.audio_analysis_manager is None:
        serv.init_manager()
    # Modify Processing status
    gv.proccesing = multiprocessing.Value('i', 1)
    serv.train_speaker_diarization_model(media)
    gv.proccesing = multiprocessing.Value('i', 0)


def train_speaker_model(media):
    response = {}
    if not gv.proccesing.value:
        # Start Thread
        p = multiprocessing.Process(target=train_speaker_model_thread, args=(media,))
        p.start()
        response = {"message": "Training Speaker model for " + media, "status": "200"}
    else:
        response = {"message": "Server is busy training the speaker model", "status": "200"}
    return response

# ==================================================================================
# -------------------------------- Calculate Embeddings ----------------------------
# ==================================================================================

def calculate_embeddings_thread(media):
    # Generate New Service
    if serv.audio_analysis_manager is None:
        serv.init_manager()
    # Modify Processing status
    gv.proccesing = multiprocessing.Value('i', 1)
    serv.generate_speaker_embeddings(media)
    gv.proccesing = multiprocessing.Value('i', 0)


def calculate_embeddings(media):
    response = {}
    if not gv.proccesing.value:
        # Start Thread
        p = multiprocessing.Process(target=calculate_embeddings_thread, args=(media,))
        p.start()
        response = {"message": "Calculate speaker Embeddings for " + media, "status": "200"}
    else:
        response = {"message": "Server is busy calculating the speaker embeddings", "status": "200"}
    return response


# ==================================================================================
# -------------------------------- Analyse Episode ----------------------------
# ==================================================================================

def analyse_speakers_episode_thread(media, episode):
    # Generate New Service
    if serv.audio_analysis_manager is None:
        serv.init_manager()
    # Modify Processing status
    gv.proccesing = multiprocessing.Value('i', 1)
    serv.predict_speaker_diarization(media=media, episode=episode)
    gv.proccesing = multiprocessing.Value('i', 0)
    # Response
    response = serv.output



def analyse_speakers_episode(media, episode):
    response = {}
    if not gv.proccesing.value:
        # Start Thread
        p = multiprocessing.Process(target=analyse_speakers_episode_thread, args=(media, episode))
        p.start()
        response = {"message": "Analyse speakers for media: " +
                               media + " and episode: " + str(episode), "status": "200"}
    else:
        response = {"message": "Server is busy predicting the speakers of media: " +
                               media + " and episode: " + str(episode), "status": "200"}
    return response



# ==================================================================================
# -------------------------------- Analyse Environment SoundEpisode ----------------
# ==================================================================================
def analyse_env_sound_episode_thread(media, episode):
    # Generate New Service
    if serv.audio_analysis_manager is None:
        serv.init_manager()

    # Modify Processing status
    gv.proccesing = multiprocessing.Value('i', 1)
    serv.predict_env_sound(media=media, episode=episode)
    gv.proccesing = multiprocessing.Value('i', 0)
    # Response
    response = serv.output
    return response


def analyse_env_sound_episode(media, episode):
    response = {}
    if not gv.proccesing.value:
        # Start Thread
        p = multiprocessing.Process(target=analyse_env_sound_episode_thread, args=(media, episode))
        p.start()
        response = {"message": "Analyse environment sounds for media: " + media + " and episode: " +
                               str(episode), "status": "200"}
    else:
        response = {"message": "Server is busy training the speaker model", "status": "200"}
    return response