from contents_preprocessing.contents_preprocessing import contents_preprocessing

import threading
from helper import global_variables as gv
from flask import abort
import wget
import xml.etree.ElementTree as ET
import json
import urllib
import os
import subprocess
import time
import unidecode


def download_content(id=""):
	try:
		ET.register_namespace("","urn:mpeg:dash:schema:mpd:2011")
		url_final = "https://dinamics.ccma.cat/pvideo/media.jsp?media=video&versio=1&idint="+id+"&profile=pc&broadcast=false&format=dm"
		with urllib.request.urlopen(url_final) as url:
			
			data=json.loads(url.read().decode("latin-1"))
			series=os.listdir(os.path.join("www","media"))
			serie= unidecode.unidecode(data["informacio"]["programa"].replace(" ","_"))
			print(unidecode.unidecode(data["informacio"]["titol"]))
			info={}
			info['description']=data["informacio"]["descripcio"]
			info['title']=data["informacio"]["titol"]
			info['duration']=data["informacio"]["durada"]["milisegons"]
			info['audio_langs']=[]
			info['subtitle_langs']=[]
		
			if not serie in series:
				print("creating directory "+serie)
				os.mkdir(os.path.join("www","media",serie))
			episodes=os.listdir(os.path.join("www","media",serie))
			episode = unidecode.unidecode(data["informacio"]["slug"].replace("---","_").replace("-","_") )
			if not episode in episodes:
				print("creating directory "+serie+"/"+episode)
				os.mkdir(os.path.join("www","media",serie,episode))

			for media in data["media"]["url"]:
				if media["label"]=="720p":
					if not os.path.isfile(os.path.join("www","media",serie,episode,episode+".mp4")):
						#wget.download(media["file"],os.path.join("www","media",serie,episode,episode+".mp4"))
						print(os.path.join("www","media",serie,episode,episode+".mp4"))
						#subprocess.call(['ffmpeg','-i', os.path.join("www","media",serie,episode,episode+".mp4"),'-vn', os.path.join("www","media",serie,episode,episode+".mp3")])
				if media["label"]=="DASH":
					with urllib.request.urlopen(media["file"]) as url:

						mpd = ET.fromstring(url.read())
						PATH= media["file"].rpartition('/')[0]
						print(media["file"])
						for child in mpd.iter():
							if("AdaptationSet" in child.tag):
								if(child.attrib['mimeType']=="audio/mp4"):
									info['audio_langs'].append(child.attrib['lang'])
							if("SegmentTemplate" in child.tag):
								child.set('media',PATH+"/"+child.attrib['media'])
								child.set('initialization',PATH+"/"+child.attrib['initialization'])
								#print(child.attrib['media'])
								#print(child.attrib['initialization'])
							if("Period" in  child.tag):
								period=child
						
						if(isinstance(data["subtitols"],list)):
							for subtitels in data["subtitols"]:
								adaptation=ET.SubElement(period,"AdaptationSet")
								adaptation.set("mimeType","text/"+subtitels["format"])
								adaptation.set("lang",subtitels["iso"])
								representation=ET.SubElement(adaptation,"Representation")
								representation.set("id", "sub_"+subtitels["iso"]+"_"+subtitels["format"])
								representation.set("bandwidth", "256")
								base=ET.SubElement(representation,"BaseURL")
								base.text=subtitels["url"]
								info['subtitle_langs'].append(subtitels["iso"])
						else:

							adaptation=ET.SubElement(period,"AdaptationSet")
							adaptation.set("mimeType","text/"+data["subtitols"]["format"])
							adaptation.set("lang",data["subtitols"]["iso"])
							representation=ET.SubElement(adaptation,"Representation")
							representation.set("id", "sub_"+data["subtitols"]["iso"]+"_"+data["subtitols"]["format"])
							representation.set("bandwidth", "256")
							base=ET.SubElement(representation,"BaseURL")
							base.text=data["subtitols"]["url"]
							print(data["subtitols"]["url"])
							info['subtitle_langs'].append(data["subtitols"]["iso"])
						#tree = ET.ElementTree()
						#tree._setroot(mpd)
						#tree.write(os.path.join("www","media",serie,episode,"manifest_sbt_vtt.mpd"))
			#with open(os.path.join("www","media",serie,episode,episode+"_info.json"), 'w') as outfile:
			#	json.dump(info, outfile)
			#wget.download(data["imatges"]["url"],os.path.join("www","media",serie,episode,"thumbnail.jpg"))	
			#ÑAPA TO CALL START WITHOUT WAITING
			#data = {}
			#data = urllib.parse.urlencode(data).encode()
			#opener = urllib.request.build_opener()
			#opener.addheaders = [('x-api-key', 'vdxv8kZfTJhZ8P5p78uK8pMyyMqTvBUrHGqLGRb6bto=r')]
			#urllib.request.install_opener(opener)
			'''
			req = urllib.request.Request("http://138.4.47.33:8080/textdetection/start/"+serie+"/"+episode,data)
			print("\nhttp://138.4.47.33:8080/textdetection/start/"+serie+"/"+episode)


			response = urllib.request.urlopen(req)

			print("\nhttp://138.4.47.33:8080/textdetection/start/"+serie+"/"+episode)

			status=0	
			while status!=3 and status!=6:
				print(1)
				with urllib.request.urlopen("http://138.4.47.33:8080/textdetection/status/"+serie+"/"+episode) as url:
					print(2)
					data=json.loads(url.read().decode("utf-8"))
					status=int(data['status']);
					if(status!=3 and status!=6 ):
						print(data)
						time.sleep(600)
			'''
		response={"message":"preprocessing content","status":"200"}
	except Exception as e:
		print(e)
		abort(500)
	return response

"""
def preprocess(media="",episode=""):
	try:
		if not media in gv.video_status.keys():
			gv.video_status[media]={}
		aux=gv.video_status[media]
		aux[episode]=gv.Status.PREPROCESSING.value
		gv.video_status[media]=aux
		thread_preprocesing = threading.Thread(target=preprocess_thread, args=(media,episode))
		thread_preprocesing.start()
		response={"message":"preprocessing content","status":"200"}
	except Exception as e:
		gv.logger.error(e)
		abort(500)
	return response


def preprocess_thread(media,episode):

	gv.proccesing=multiprocessing.Value('i', 1)
	contents_preprocessing(media=media,episode=episode)
	gv.proccesing=multiprocessing.Value('i', 0)
	aux=gv.video_status[media]
	aux[episode]=gv.Status.PREPROCESSED.value
	gv.video_status[media]=aux
"""