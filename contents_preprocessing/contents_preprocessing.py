
#15/04/2019

#Usage:
#python preprocessing.py --media_path --mpd_name


#import the necessary packages
import subprocess
import os
from helper import global_variables as gv
def contents_preprocessing(media="",episode=""):


	try:
		media_path=os.path.join("www","media",media,episode)

		#Inputs/Outputs
		mpd_name= "manifest_sbt.mpd"
		#Names
		i_mpd=os.path.join(media_path, mpd_name)
		o_video=episode + ".mp4"
		o_video_path=os.path.join(media_path,o_video)
		o_audio=episode + ".mp3"
		o_audio_path=os.path.join(media_path,o_audio)
		o_image="thumbnail.jpg"
		o_image_path=os.path.join(media_path,o_image)

		print("Location MPD: " +i_mpd)
		print("Location video: " +o_video_path)
		print("Location audio: " +o_audio_path)
		print("Location image: " +o_image_path)
		print("video: "+o_video)
		print("audio: "+o_audio)
		print("image: "+o_image)

		#Generating the video
		if(not os.path.isfile(o_video_path)):
			subprocess.call(['ffmpeg','-allowed_extensions','ALL','-i', i_mpd,'-c:v', 'copy', '-c:a','copy', o_video_path])

		#Generating the audio
		if(not os.path.isfile(o_audio_path)):
			subprocess.call(['ffmpeg','-allowed_extensions','ALL','-i', i_mpd,'-vn', o_audio_path])
		  
		#Generating the image
		if(not os.path.isfile(o_image_path)):
			subprocess.call(['ffmpeg','-allowed_extensions','ALL','-i', i_mpd, '-ss', '00:00:03','-vframes','1', o_image_path])

	except Exception as e:
		gv.logger.error(e)




    
    
    
    
    
