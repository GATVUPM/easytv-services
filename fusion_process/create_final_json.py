import json
import glob
import numpy as np
import time
import os
import cv2


def fusion_process(video_path=None):

	def most_frequent(List): 
		counter = 0
		num = List[0] 
		for i in List: 
			curr_frequency = List.count(i) 
			if(curr_frequency> counter): 
				counter = curr_frequency 
				num = i 

		return num 

	def getOverlap(a, b, c, d):
		return max(0, min(a, b) - max(c, d))

	# Model face if exists
	path = video_path.split("/")
	init_path = ""
	for i, p in enumerate(path):
		if i != len(path)-2:
			init_path += p
			init_path += "/"
		else:
			break

	path = video_path.split("/")
	name_json_video = path[-1].split(".")[0]
	json_path = ""
	for i, p in enumerate(path):
		if i != len(path)-1:
			json_path += p
			json_path += "/"
		else:
			json_path += name_json_video
			break

	characters = []
	for _, dirnames, filenames in os.walk(init_path+'Dataset/images'):
		for name in dirnames:
			characters.append(name)

	cam = cv2.VideoCapture(video_path)
	fps = cam.get(cv2.CAP_PROP_FPS)
	cam.release()

	with open(json_path+"_faces_final.json") as json_file:  
		data_video = json.load(json_file)

	with open(json_path+"_speaker_dir.json") as json_file:  
		data_audio = json.load(json_file)

	conf_img = np.load(init_path+"Dataset/confusion_matrix_image.npy")
	conf_aud = np.load(init_path+"Dataset/confusion_matrix_audio.npy")

	conf_img_div = np.array([np.sum(conf_img, axis=1)]).T
	conf_img = np.nan_to_num(conf_img/conf_img_div)

	conf_aud_div = np.array([np.sum(conf_aud, axis=1)]).T
	conf_aud = np.nan_to_num(conf_aud/conf_aud_div)

	#################################################################################

	print("Ordering Data")

	final_json = {"scene_changes": [], "characters": [], "speaking": []}
	vec_scenes = []
	for i, scene in enumerate(data_video['scene_change']):
		if scene == 1:
			vec_scenes.append(i)
			#print(vec_scenes[-1])

			names = []
			faces_scene = []
			for j, face in enumerate(data_video['faces']):
				
				if len(list(face.keys()))>0:

					if int(list(face.keys())[0]) == len(vec_scenes):
						if face[str(len(vec_scenes))]["name2"] not in names: # and face[str(len(vec_scenes))]["name2"] in characters:
							names.append(face[str(len(vec_scenes))]["name2"])
						faces_scene.append(face[str(len(vec_scenes))])
			
			if len(names)>0:

				age = []
				gender = []
				names_probs = []

				characters_data = []
				for name in names:
					for face in faces_scene:
						if face["name2"] == name:
							age.append(face["age"])
							gender.append(face["gender"])
							names_probs.append(face["name"])


					age_mean = int(np.mean(np.array(age)))
					gender_most = most_frequent(gender)

					characters_data.append({"name": names_probs, "gender": gender_most, "age": age_mean, "id": face["name2"]})


				track_list = {str(i): None}
				dict_track = {}

				for ind, face in enumerate(faces_scene):
					if face["speak"][0] == 1:
						dict_track[str(i+ind)] = {"x": face["x"], "y": face["y"], "w": face["w"], "h": face["h"], "character": face["name2"]} #characters.index(face["name2"])}

				final_json["characters"].append(characters_data)
				final_json["speaking"].append(dict_track)
				final_json["scene_changes"].append(i)

	####### Version solo caras #######

	'''with open('final_json.json') as json_file:
		final_json = json.load(json_file)

	for s, scene in enumerate(final_json["scene_changes"]):

		for i, char in enumerate(final_json["characters"][s]):

			names_probs = char["name"]

			list_max = [0] * len(characters)
			for prob in names_probs:
				ind = np.argmax(prob)
				list_max[ind] += 1

			name = characters[np.argmax(list_max)]

			final_json["characters"][s][i]["name"] = name

			id_comp = final_json["characters"][s][i]["id"]

			for keys_frame in list(final_json["speaking"][s].keys()):
				if final_json["speaking"][s][keys_frame]['character'] == id_comp:
					final_json["speaking"][s][keys_frame]['character'] = name

			final_json["characters"][s][i].pop('id')


	all_characters = []
	ind_unk = 0
	names_app = []
	for i, chars in enumerate(final_json["characters"]):

		for char in chars:
			if char["name"] not in names_app and char["name"]!= "Unknown":
				all_characters.append(char)
				names_app.append(char["name"])

			elif char["name"] == "Unknown":

				char_unk = char
				char_unk["name"] = "Unknown "+str(ind_unk)
				all_characters.append(char_unk)
				ind_unk += 1

	ind_unk = 0
	name_unk = "Unknown "+ str(ind_unk)
	for i, part in enumerate(final_json["speaking"]):

		names = []
		if len(list(part.keys()))!=0:
			frame_ant = int(list(part.keys())[0])
		for j, p in enumerate(list(part.keys())):

			if (int(p)-frame_ant != 1 or int(p)-frame_ant != 0) and final_json["speaking"][i][p]["character"]=="Unknown":
				name_unk = "Unknown " + str(ind_unk)
				ind_unk += 1
				names.append(name_unk)

			if final_json["speaking"][i][p]["character"] not in names and final_json["speaking"][i][p]["character"]!="Unknown":
				names.append(final_json["speaking"][i][p]["character"])

			elif final_json["speaking"][i][p]["character"]=="Unknown" and not unk_enter:
				final_json["speaking"][i][p]["character"] = name_unk

			frame_ant = int(p)

		for j, p in enumerate(list(part.keys())):
			name = final_json["speaking"][i][p]["character"]
			for l, chars in enumerate(all_characters):
				if chars["name"] == name:
					final_json["speaking"][i][p]["character"] = l

		list_names = []
		for n in names:
			for l, chars in enumerate(all_characters):
				if chars["name"] == n:
					list_names.append(l)

		final_json["speaking"][i]["characters"] = list_names


	final_json["characters"] = all_characters


	with open('final_json_faces.json', 'w') as outfile:
		json.dump(final_json, outfile)'''

	########################################################################

	print("Fusing Information")

	list_audio = []
	for audio_data in data_audio["Output"]:
		start = int(audio_data["start"]*fps)
		end = int(audio_data["end"]*fps)
		list_audio.append({"speaker": audio_data["probabilities"], "start": start, "end": end})

	for i in range(len(final_json["scene_changes"])):

		intervals = []

		ind_person = 0
		for j, face in enumerate(list(final_json["speaking"][i].keys())):

			if j == 0:
				start = int(face)
				speaker = final_json["characters"][i][ind_person]
				aux_speaker = np.zeros(len(characters))
				for speak in speaker["name"]:
					aux_speaker = aux_speaker + np.array(speak)

				speaker = (aux_speaker/np.max(aux_speaker)).tolist()

				end = int(face)

			else:
				if int(face) == end+1:
					end = int(face)

				else:
					intervals.append({"speaker": speaker, "start": start, "end": end})
					start = int(face)
					end = int(face)
					ind_person += 1

			if j == len(list(final_json["speaking"][i].keys()))-1:
				intervals.append({"speaker": speaker, "start": start, "end": end})

		##### Version 1 tomar caras como la clave y despreciar lo demas

		solapes = []
		for interval in intervals:
			audio_solape = np.zeros(len(characters))
			n_sol = 0
			for audio_data in list_audio:
				if getOverlap(interval["end"], audio_data["end"], interval["start"], audio_data["start"]):
					audio_solape = audio_solape + np.array(audio_data["speaker"])
					n_sol += 1

			if n_sol>0:
				audio_solape = audio_solape/n_sol

			mults = np.mean(interval["speaker"]*conf_img, axis=0) + np.mean(audio_solape*conf_aud, axis=0)
			character_fusion = characters[np.argmax(mults)]

			#print(np.argmax(mults), character_fusion)

			interval["speaker"] = character_fusion
			solapes.append(interval)

		names_norep = []
		for sol in solapes:
			if sol["speaker"] not in names_norep:
				names_norep.append(sol["speaker"])

		for n, names_r in enumerate(names_norep):
			final_json["characters"][i][n]["name"] = names_r

		for sol in solapes:
			for keys_frame in list(final_json["speaking"][i].keys()):
				if int(keys_frame)>=sol["start"] and int(keys_frame)<=sol["end"]:
					final_json["speaking"][i][keys_frame]["character"] = sol["speaker"]

	##### Version 2 tomar caras como la clave y audios sin solape como independientes

	##### Version 3 audios como clave


	for s, scene in enumerate(final_json["scene_changes"]):

		for i, char in enumerate(final_json["characters"][s]):

			names_probs = char["name"]

			list_max = [0] * len(characters)
			for prob in names_probs:
				ind = np.argmax(prob)
				list_max[ind] += 1

			name = characters[np.argmax(list_max)]

			final_json["characters"][s][i]["name"] = name

			id_comp = final_json["characters"][s][i]["id"]

			for keys_frame in list(final_json["speaking"][s].keys()):
				if final_json["speaking"][s][keys_frame]['character'] == id_comp:
					final_json["speaking"][s][keys_frame]['character'] = name

			final_json["characters"][s][i].pop('id')


	all_characters = []
	ind_unk = 0
	names_app = []
	for i, chars in enumerate(final_json["characters"]):

		for char in chars:
			if char["name"] not in names_app and char["name"]!= "Unknown":
				all_characters.append(char)
				names_app.append(char["name"])

			elif char["name"] == "Unknown":

				char_unk = char
				char_unk["name"] = "Unknown "+str(ind_unk)
				all_characters.append(char_unk)
				ind_unk += 1

	ind_unk = 0
	name_unk = "Unknown "+ str(ind_unk)
	for i, part in enumerate(final_json["speaking"]):

		names = []
		if len(list(part.keys()))!=0:
			frame_ant = int(list(part.keys())[0])
		for j, p in enumerate(list(part.keys())):

			if (int(p)-frame_ant != 1 or int(p)-frame_ant != 0) and final_json["speaking"][i][p]["character"]=="Unknown":
				name_unk = "Unknown " + str(ind_unk)
				ind_unk += 1
				names.append(name_unk)

			if final_json["speaking"][i][p]["character"] not in names and final_json["speaking"][i][p]["character"]!="Unknown":
				names.append(final_json["speaking"][i][p]["character"])

			elif final_json["speaking"][i][p]["character"]=="Unknown" and not unk_enter:
				final_json["speaking"][i][p]["character"] = name_unk

			frame_ant = int(p)

		for j, p in enumerate(list(part.keys())):
			name = final_json["speaking"][i][p]["character"]
			for l, chars in enumerate(all_characters):
				if chars["name"] == name:
					final_json["speaking"][i][p]["character"] = l

		list_names = []
		for n in names:
			for l, chars in enumerate(all_characters):
				if chars["name"] == n:
					list_names.append(l)

		final_json["speaking"][i]["characters"] = list_names


	final_json["characters"] = all_characters

	with open(json_path+'_final_json_fusion1.json', 'w') as outfile:
		json.dump(final_json, outfile)

	print("Final json file created!")