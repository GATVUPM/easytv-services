import json
import glob
import numpy as np
import time
import cv2

def most_frequent(List): 
    counter = 0
    num = List[0] 
      
    for i in List: 
        curr_frequency = List.count(i) 
        if(curr_frequency> counter): 
            counter = curr_frequency 
            num = i 
  
    return num 

def getOverlap(a, b, c, d):
    return max(0, min(a, b) - max(c, d))


characters = ["Alba", "Andreu", "Anna", "Cati", "Clara", "Dolors", "Elisabet", "Eva", "Gemma", "Helena", "Ismael", "Ivan", "Joana", "Jordi", "Litus", "Lola", "Marc", "Mari Carmen", "Marta", "Mart\u00ed", "Miquel", "Noe", "Paz", "Pepo", "Pili", "Quim", "Sandra", "Santi", "Silvia", "Txell", "Unknown", "Vicen\u00e7", "Xavi"]


video = cv2.VideoCapture("com_si_fos_ahir_capitol_427.mp4")
fps = video.get(cv2.CAP_PROP_FPS)
_, frame = video.read()
hv, wv, _ = frame.shape

#fps = 25

files = glob.glob("*.json")

for file in files:

    if "faces_final" in file:
        file = "com_si_fos_ahir_capitol_428_faces_final.json"
        with open(file) as json_file:
            data_video = json.load(json_file)

    if "speaker_dir" in file:
        with open(file) as json_file:
            data_audio = json.load(json_file)

conf_img = np.load("confusion_matrix_image.npy")
conf_aud = np.load("confusion_matrix_audio.npy")


#conf_aud_div = np.array([np.sum(conf_aud, axis=1)]).T
#conf_aud = np.nan_to_num(conf_aud/conf_aud_div)
#conf_aud = np.save("confusion_matrix_audio.npy", conf_aud)

#dict_keys(['scene_change', 'faces'])
#print(data_video.keys())
#print(data_video["faces"])

############################################################################
final_json = {"scene_changes": [], "characters": [], "speaking": []}
vec_scenes = []
vec_frames = []

n = 0
for i, scene in enumerate(data_video['scene_change']):
    if scene == 1:
        vec_scenes.append(i)
        vec_frames.append(n)
        n += 1
        #print(vec_scenes[-1])
      
vec_post = 0
for i, scene in enumerate(vec_scenes):
    
    if i == len(vec_scenes)-1:
        vec_post = 9999999999999999999999999999
    else: 
        vec_post = vec_scenes[i+1]-1

    names = []
    faces_scene = []
    
    print("Processing Scene "+ str(scene) + "-" + str(vec_post))
    
    for j, face in enumerate(data_video['faces']):
        #if j<scene:
        #    continue
        
        #if j>vec_post:
        #    break
        
        #print(face)
        #time.sleep(2)
            
        if len(list(face.keys()))>0:
            
            if len(list(face.keys()))>1:

                for l, face_un in enumerate(list(face.keys())):
                    if face[face_un]["speak"][0] != 1:
                        face.pop(face_un)
            
            if len(list(face.keys()))==0:
                continue
            
            #print(face, int(list(face.keys())[0]), scene, int(list(face.keys())[0]), vec_post)
            #if int(list(face.keys())[0]) == int(vec_frames[i]):
            if j>=scene and j<=vec_post:
                
                #print(int(list(face.keys())[0]), vec_frames[i])
               
                if face[list(face.keys())[0]]["name2"] not in names: # and face[str(len(vec_scenes))]["name2"] in characters:
                    names.append(face[list(face.keys())[0]]["name2"])
                
                faces_scene.append([face[list(face.keys())[0]], j])
    
    #print(faces_scene)
        
    if len(names)>0:
        
        age = []
        gender = []
        names_probs = []

        characters_data = []
        for name in names:
            for face in faces_scene:
                if face[0]["name2"] == name:
                    age.append(face[0]["age"])
                    gender.append(face[0]["gender"])
                    names_probs.append(face[0]["name"])

            age_mean = int(np.mean(np.array(age)))
            gender_most = most_frequent(gender)

            characters_data.append({"name": names_probs, "gender": gender_most, "age": age_mean, "id": face[0]["name2"], })


        track_list = {str(i): None}
        dict_track = {}

        for ind, face in enumerate(faces_scene):
            #if face[0]["speak"][0] == 1:

            norm_x = (float(face[0]["x"])+float(face[0]["w"])/2)/wv
            norm_y = (float(face[0]["y"])+float(face[0]["h"])/2)/hv

            dict_track[face[1]] = {"x": norm_x, "y": norm_y, "character": face[0]["name2"]} #characters.index(face["name2"])}
            #dict_track[str(i+ind)] = {"x": face["x"], "y": face["y"], "w": face["w"], "h": face["h"], "character": face["name2"]} #characters.index(face["name2"])}

        #print(dict_track)

        final_json["characters"].append(characters_data)
        final_json["speaking"].append(dict_track)
        final_json["scene_changes"].append(scene)
        
        #print(final_json["characters"])

        #print(final_json)
        #time.sleep(10)



with open('final_json.json', 'w') as outfile:
    json.dump(final_json, outfile)

####### Version solo caras #######
    
print("Faces process")

with open('final_json.json') as json_file:
    final_json = json.load(json_file)

for s, scene in enumerate(final_json["scene_changes"]):

    for i, char in enumerate(final_json["characters"][s]):

        names_probs = char["name"]

        list_max = np.array([0] * len(characters))
        for prob in names_probs:
            #ind = np.argmax(prob)
            #list_max[ind] += 1
            list_max = list_max+np.array(prob)

        name = characters[np.argmax(list_max/len(names_probs))]

        final_json["characters"][s][i]["name"] = name

        id_comp = final_json["characters"][s][i]["id"]

        for keys_frame in list(final_json["speaking"][s].keys()):
            if final_json["speaking"][s][keys_frame]['character'] == id_comp:
                final_json["speaking"][s][keys_frame]['character'] = name

        final_json["characters"][s][i].pop('id')

'''all_characters = []
ind_unk = 0
names_app = []
for i, chars in enumerate(final_json["characters"]):

    for char in chars:
        if char["name"] not in names_app and char["name"]!= "Unknown":
            all_characters.append(char)
            names_app.append(char["name"])

        elif char["name"] == "Unknown":

            char_unk = char
            char_unk["name"] = "Unknown "+str(ind_unk)
            all_characters.append(char_unk)
            ind_unk += 1

ind_unk = 0
name_unk = "Unknown "+ str(ind_unk)
for i, part in enumerate(final_json["speaking"]):

    names = []
    if len(list(part.keys()))!=0:
        frame_ant = int(list(part.keys())[0])

    for j, p in enumerate(list(part.keys())):

        if (int(p)-frame_ant != 1 or int(p)-frame_ant != 0) and final_json["speaking"][i][p]["character"]=="Unknown":
            name_unk = "Unknown " + str(ind_unk)
            ind_unk += 1
            names.append(name_unk)
            print(name_unk)

        if final_json["speaking"][i][p]["character"] not in names and final_json["speaking"][i][p]["character"]!="Unknown":
            names.append(final_json["speaking"][i][p]["character"])

        elif final_json["speaking"][i][p]["character"]=="Unknown":

            indc = -1
            for o, charac in enumerate(all_characters):
                if charac["name"] == name_unk:
                    indc = o
                    break

            final_json["speaking"][i][p]["character"] = indc

        frame_ant = int(p)

    for j, p in enumerate(list(part.keys())):
        name = final_json["speaking"][i][p]["character"]
        for l, chars in enumerate(all_characters):
            if chars["name"] == name:
                final_json["speaking"][i][p]["character"] = l

    list_names = []
    for n in names:
        for l, chars in enumerate(all_characters):
            if chars["name"] == n:
                list_names.append(l)

    final_json["speaking"][i]["characters"] = list_names


final_json["characters"] = all_characters


with open('final_json_faces2.json', 'w') as outfile:
    json.dump(final_json, outfile)'''


all_characters = []
ind_unk = 0
names_app = []
for i, chars in enumerate(final_json["characters"]):

    for char in chars:
        if char["name"] not in names_app and char["name"]:
            all_characters.append(char)
            names_app.append(char["name"])


ind_unk = 0
name_unk = "Unknown "+ str(ind_unk)
for i, part in enumerate(final_json["speaking"]):

    names = []

    for j, p in enumerate(list(part.keys())):

        if final_json["speaking"][i][p]["character"] not in names:        
            names.append(final_json["speaking"][i][p]["character"])

    for j, p in enumerate(list(part.keys())):
        name = final_json["speaking"][i][p]["character"]
        for l, chars in enumerate(all_characters):
            if chars["name"] == name:
                final_json["speaking"][i][p]["character"] = l

    list_names = []
    for n in names:
        for l, chars in enumerate(all_characters):
            if chars["name"] == n:
                list_names.append(l)

    final_json["speaking"][i]["characters"] = list_names

final_json["characters"] = all_characters



'''
with open('final_json.json') as json_file:
    final_json_old = json.load(json_file)

for data in final_json["speaking"]:

    for j, face in enumerate(data_video['faces']):


if j>=scene and j<=vec_post:


        for k, key_face in enumerate(list(face.keys())):

            if key_face in list(data.keys()):'''


vec_post = 0
for i, scene in enumerate(final_json["scene_changes"]):
    
    if i == len(final_json["scene_changes"])-1:
        vec_post = 9999999999999999999999999999
    else: 
        vec_post = final_json["scene_changes"][i+1]-1

    names = []
    faces_scene = []
    
    print("Processing Scene "+ str(scene) + "-" + str(vec_post))
    
    for j, face in enumerate(data_video['faces']):

        if len(list(face.keys()))>0:
            if j>=scene and j<=vec_post:
 
                if face[list(face.keys())[0]]["name2"] not in names: # and face[str(len(vec_scenes))]["name2"] in characters:
                    names.append(face[list(face.keys())[0]]["name2"])
                
                faces_scene.append([face[list(face.keys())[0]], j])

    if len(names)>0:
        
        age = []
        gender = []
        names_probs = []

        characters_data = []
        for name in names:
            for face in faces_scene:
                if face[0]["name2"] == name:
                    age.append(face[0]["age"])
                    gender.append(face[0]["gender"])
                    names_probs.append(face[0]["name"])

            list_max = np.array([0] * len(characters))
            for prob in names_probs:
                list_max = list_max+np.array(prob)

            name_pred = characters[np.argmax(list_max/len(names_probs))]

            age_mean = int(np.mean(np.array(age)))
            gender_most = most_frequent(gender)

            names_charac = []
            for charac in final_json["characters"]:
                names_charac.append(charac["name"])

            if name_pred not in names_charac:
                final_json["characters"].append({"name": name_pred, "gender": gender_most, "age": age_mean})
                final_json["speaking"][i]["characters"].append(len(names_charac))

            else:
                index_name = names_charac.index(name_pred)
                if index_name not in final_json["speaking"][i]["characters"]:
                    final_json["speaking"][i]["characters"].append(index_name)

        if len(final_json["speaking"][i]["characters"])>1:
            print(final_json["speaking"][i]["characters"])



with open('final_json_faces2.json', 'w') as outfile:
    json.dump(final_json, outfile)

########################################################################

'''
with open('final_json.json') as json_file:
    final_json = json.load(json_file)

list_audio = []
for audio_data in data_audio["Output"]:
    start = int(audio_data["start"]*fps)
    end = int(audio_data["end"]*fps)
    list_audio.append({"speaker": audio_data["probabilities"], "start": start, "end": end})

for i in range(len(final_json["scene_changes"])):

    intervals = []

    ind_person = 0
    for j, face in enumerate(list(final_json["speaking"][i].keys())):

        if j == 0:
            start = int(face)
            speaker = final_json["characters"][i][ind_person]
            aux_speaker = np.zeros(len(characters))
            for speak in speaker["name"]:
                aux_speaker = aux_speaker + np.array(speak)

            speaker = (aux_speaker/np.max(aux_speaker)).tolist()

            end = int(face)

        else:
            if int(face) == end+1:
                end = int(face)

            else:
                intervals.append({"speaker": speaker, "start": start, "end": end})
                start = int(face)
                end = int(face)
                ind_person += 1

        if j == len(list(final_json["speaking"][i].keys()))-1:
            intervals.append({"speaker": speaker, "start": start, "end": end})

    ##### Version 1 tomar caras como la clave y despreciar lo demas

    solapes = []
    for interval in intervals:
        audio_solape = np.zeros(len(characters))
        n_sol = 0
        for audio_data in list_audio:
            if getOverlap(interval["end"], audio_data["end"], interval["start"], audio_data["start"]):
                audio_solape = audio_solape + np.array(audio_data["speaker"])
                n_sol += 1

        if n_sol>0:
            audio_solape = audio_solape/n_sol

        mults = np.mean(interval["speaker"]*conf_img, axis=0) + np.mean(audio_solape*conf_aud, axis=0)
        character_fusion = characters[np.argmax(mults)]

        #print(np.argmax(mults), character_fusion)

        interval["speaker"] = character_fusion
        solapes.append(interval)

    names_norep = []
    for sol in solapes:
        if sol["speaker"] not in names_norep:
            names_norep.append(sol["speaker"])

    for n, names_r in enumerate(names_norep):
        final_json["characters"][i][n]["name"] = names_r

    for sol in solapes:
        for keys_frame in list(final_json["speaking"][i].keys()):
            if int(keys_frame)>=sol["start"] and int(keys_frame)<=sol["end"]:
                final_json["speaking"][i][keys_frame]["character"] = sol["speaker"]

##### Version 2 tomar caras como la clave y audios sin solape como independientes

##### Version 3 audios como clave


for s, scene in enumerate(final_json["scene_changes"]):

    for i, char in enumerate(final_json["characters"][s]):

        names_probs = char["name"]

        list_max = [0] * len(characters)
        for prob in names_probs:
            ind = np.argmax(prob)
            list_max[ind] += 1

        name = characters[np.argmax(list_max)]

        final_json["characters"][s][i]["name"] = name

        id_comp = final_json["characters"][s][i]["id"]

        for keys_frame in list(final_json["speaking"][s].keys()):
            if final_json["speaking"][s][keys_frame]['character'] == id_comp:
                final_json["speaking"][s][keys_frame]['character'] = name

        final_json["characters"][s][i].pop('id')


all_characters = []
ind_unk = 0
names_app = []
for i, chars in enumerate(final_json["characters"]):

    for char in chars:
        if char["name"] not in names_app and char["name"]!= "Unknown":
            all_characters.append(char)
            names_app.append(char["name"])

        elif char["name"] == "Unknown":

            char_unk = char
            char_unk["name"] = "Unknown "+str(ind_unk)
            all_characters.append(char_unk)
            ind_unk += 1

ind_unk = 0
name_unk = "Unknown "+ str(ind_unk)
for i, part in enumerate(final_json["speaking"]):

    names = []
    if len(list(part.keys()))!=0:
        frame_ant = int(list(part.keys())[0])
    for j, p in enumerate(list(part.keys())):

        if (int(p)-frame_ant != 1 or int(p)-frame_ant != 0) and final_json["speaking"][i][p]["character"]=="Unknown":
            name_unk = "Unknown " + str(ind_unk)
            ind_unk += 1
            names.append(name_unk)

        if final_json["speaking"][i][p]["character"] not in names and final_json["speaking"][i][p]["character"]!="Unknown":
            names.append(final_json["speaking"][i][p]["character"])

        elif final_json["speaking"][i][p]["character"]=="Unknown" and not unk_enter:
            final_json["speaking"][i][p]["character"] = name_unk

        frame_ant = int(p)

    for j, p in enumerate(list(part.keys())):
        name = final_json["speaking"][i][p]["character"]
        for l, chars in enumerate(all_characters):
            if chars["name"] == name:
                final_json["speaking"][i][p]["character"] = l

    list_names = []
    for n in names:
        for l, chars in enumerate(all_characters):
            if chars["name"] == n:
                list_names.append(l)

    final_json["speaking"][i]["characters"] = list_names


final_json["characters"] = all_characters


with open('final_json_fusion1.json', 'w') as outfile:
    json.dump(final_json, outfile)'''