import numpy as np
import cv2
import json
import sys
import os

def text_subs(video_path=None):

	path = video_path.split("/")
	name_json_video = path[-1].split(".")[0]
	json_path = ""
	for i, p in enumerate(path):
		if i != len(path)-1:
			json_path += p
			json_path += "/"
		else:
			json_path += name_json_video
			break

	with open(json_path+"_text_process.json") as json_file:  
		final_sentences = json.load(json_file)

	cam = cv2.VideoCapture(video_path)
	fps = cam.get(cv2.CAP_PROP_FPS)

	file = open(json_path+"_text_subs.vtt","w") 

	for ind, sentence in enumerate(final_sentences):

		if ind == 0:
			file.write("WEBVTT\n")
			file.write("\n")
			'''file.write("Region: id=r1 width=100% lines=3 regionanchor=0%,96% viewportanchor=0%,100% scroll=none\n")
			file.write("Region: id=r2 width=100% lines=1 regionanchor=0%,96% viewportanchor=0%,100% scroll=none\n")
			file.write("Region: id=r3 width=100% lines=3 regionanchor=0%,88% viewportanchor=0%,100% scroll=none\n")
			file.write("Region: id=r4 width=100% lines=1 regionanchor=0%,79% viewportanchor=0%,100% scroll=none\n")
			file.write("Region: id=r5 width=98% lines=2 regionanchor=3%,79% viewportanchor=0%,100% scroll=none\n")
			file.write("Region: id=r6 width=100% lines=2 regionanchor=0%,96% viewportanchor=0%,100% scroll=none\n") 
			file.write("Region: id=r7 width=100% lines=3 regionanchor=0%,88% viewportanchor=0%,100% scroll=none\n")
			file.write("Region: id=r8 width=100% lines=1 regionanchor=0%,88% viewportanchor=0%,100% scroll=none\n")
			file.write("Region: id=r9 width=100% lines=2 regionanchor=0%,88% viewportanchor=0%,100% scroll=none\n")
			file.write("Region: id=r10 width=100% lines=3 regionanchor=0%,88% viewportanchor=0%,100% scroll=none\n") 
			file.write("Region: id=r11 width=100% lines=2 regionanchor=0%,79% viewportanchor=0%,100% scroll=none\n")
			file.write("Region: id=r12 width=100% lines=3 regionanchor=0%,79% viewportanchor=0%,100% scroll=none\n")
			file.write("Region: id=r13 width=98% lines=1 regionanchor=3%,96% viewportanchor=0%,100% scroll=none\n")
			file.write("\n")'''

		sec1 = int(sentence[1][0]/fps)
		milsec1 = (sentence[1][0]-sec1*fps)/fps
		print(milsec1)

		hours1 = int(sec1/3600)
		mins1 = int(((sec1/3600) - int(sec1/3600))*60)
		secs1 = int(sec1-(hours1*60*60+mins1*60))

		sec2 = int(sentence[1][1]/fps)
		milsec2 = (sentence[1][1]-sec1*fps)/fps

		hours2 = int(sec2/3600)
		mins2 = int(((sec2/3600) - int(sec2/3600))*60)
		secs2 = int(sec2-(hours2*60*60+mins2*60))

		print(sec1, milsec1, sec2, milsec2)

		#file.write(str(ind+1)+"\n")
		if hours1+mins1+secs1+milsec1 == 0:
			date1 = "00:00:00."
			mils1 = "010"
		else:
			date1 = str(hours1).zfill(2)+":"+str(mins1).zfill(2)+":"+str(secs1).zfill(2)+"."
			mils1 = str(int(milsec1)).zfill(3)
		mid = " --> "
		date2 = str(hours2).zfill(2)+":"+str(mins2).zfill(2)+":"+str(secs2).zfill(2)+"."
		mils2 = str(int(milsec2)).zfill(3)
		last = " region:r1 line:88% align:center\n"
		file.write(date1+mils1+mid+date2+mils2+"\n")#+last)

		text_part1 = "<c.white.background-black>"
		text_part2 = "</c>\n"
		#file.write(text_part1+sentence[0]+text_part2)
		file.write(sentence[0]+"\n")

		if ind != len(final_sentences)-1:
			file.write("\n")

	file.close() 
	cam.release()


if __name__ == '__main__':
	text_subs(video_path=str(sys.argv[1]))