import cv2
import os
import shutil
import sys
import time

from .nets import model_train as model
from .utils.rpn_msr.proposal_layer import proposal_layer
from .utils.text_connector.detectors import TextDetector
from .utils.text_process.process_text import *

import numpy as np
import pytesseract

sys.path.append(os.getcwd())
print(sys.path.append(os.getcwd()))

import math
from PIL import Image
import json

import pickle

import os

abs_path = os.path.abspath(os.path.dirname(__file__))+"/"

import tensorflow as tf
graph = tf.get_default_graph()



def text_extraction(video_path=None):
	global abs_path, graph

	with graph.as_default():

		global_step = tf.get_variable('global_step', [], initializer=tf.constant_initializer(0), trainable=False)

		input_image = tf.placeholder(tf.float32, shape=[None, None, None, 3], name='input_image')
		input_im_info = tf.placeholder(tf.float32, shape=[None, 3], name='input_im_info')

		bbox_pred, cls_pred, cls_prob = model.model(input_image)

		#global graph, abs_path, created, global_step, input_image, input_im_info, bbox_pred, cls_pred, cls_prob, sess

		sess = tf.Session(config=tf.ConfigProto(allow_soft_placement=True))

		variable_averages = tf.train.ExponentialMovingAverage(0.997, global_step)
		saver = tf.train.Saver(variable_averages.variables_to_restore())
		saver.restore(sess, os.path.join(abs_path+'checkpoints_mlt/', os.path.basename(tf.train.get_checkpoint_state(abs_path+'checkpoints_mlt/').model_checkpoint_path)))

		#tf.app.flags.DEFINE_string('gpu', '0', '')
		#tf.app.flags.DEFINE_string('checkpoint_path', abs_path+'checkpoints_mlt/', '')

		path = video_path.split("/")
		name_json_video = path[-1].split(".")[0]
		json_path = ""
		for i, p in enumerate(path):
			if i != len(path)-1:
				json_path += p
				json_path += "/"
			else:
				json_path += name_json_video
				break
		if os.path.isfile(json_path+"_text_settings.json"):
			with open(json_path+"_text_settings.json") as json_file:  
				boxes_sel = json.load(json_file)
		else:
			boxes_sel=[]
		# Text detection
		#os.environ['CUDA_VISIBLE_DEVICES'] = FLAGS.gpu

		global_data = {"bbox": [], "text": []}

		config = ("-l spa --oem 1 --psm 7")

		vs = cv2.VideoCapture(video_path)
		width_video = vs.get(3)   # float
		height_video = vs.get(4) # float

		ind_f = 0
		while True:
			# Read frame


			ret, frameOr = vs.read()

			if not ret:
				break

			#if ind_f // 2 == 0:
			#	continue
			
			#if frameOr is None or ind_f >= 300:
			#	global_data["bbox"].append([])
			#	global_data["text"].append([])
			#	continue

			ind_f += 1

			frameDraw = frameOr.copy()

			cut_images = []
			displacements = []
			if len(boxes_sel) == 0:
				cut_images.append(frameOr.copy())
				displacements.append([0,0])
			else:
				for boxx in boxes_sel:
					cut_images.append(frameOr[int(boxx["y0"]*height_video):int(boxx["y1"]*height_video),int(boxx["x0"]*width_video):int(boxx["x1"]*width_video)])
					displacements.append([int(boxx["x0"]*width_video),int(boxx["y0"]*height_video)])

			data_bb2 = []
			data_text2 = []

			print('===============')
			for l, frame in enumerate(cut_images):
				start = time.time()

				# Adapt frame and predict
				im = frame[:, :, ::-1]
				img, (rh, rw) = resize_image(im)
				h, w, c = img.shape
				im_info = np.array([h, w, c]).reshape([1, 3])
				bbox_pred_val, cls_prob_val = sess.run([bbox_pred, cls_prob], feed_dict={input_image: [img], input_im_info: im_info})

				textsegs, _ = proposal_layer(cls_prob_val, bbox_pred_val, im_info)
				scores = textsegs[:, 0]
				textsegs = textsegs[:, 1:5]

				textdetector = TextDetector(DETECT_MODE='H')
				boxes = textdetector.detect(textsegs, scores[:, np.newaxis], img.shape[:2])
				boxes = np.array(boxes, dtype=np.int)

				cost_time = (time.time() - start)
				print("cost time: {:.2f}s".format(cost_time))

				img = cv2.resize(img, None, None, fx=1.0 / rh, fy=1.0 / rw, interpolation=cv2.INTER_LINEAR)
				img_fin = img[:, :, ::-1]

				# Text from bounding boxes
				data_bb = []
				data_text = []
				for i, box in enumerate(boxes):
					print(box)

					#try:
					badbox = False
					config = ("-l eng --oem 1 --psm 7")
					for x in [5,4,3,2,1,0]:
						try:
							roi = frame[math.ceil(box[1]*(1.0/rh))-x:math.ceil(box[5]*(1.0/rh))+x, math.ceil(box[0]*(1.0/rw))-x:math.ceil(box[4]*(1.0/rw))+x]
						except:
							if x==0:
								badbox = True
							continue

					if badbox:
						continue

					roi = cv2.cvtColor(roi,cv2.COLOR_BGR2RGB)
					roi = Image.fromarray(roi)

					text = pytesseract.image_to_string(roi, config=config)
					print(text)
					data_text.append(text)

					#except:
					#	continue

					box = box.astype(np.int32)
					box[:8] = box[:8]*np.array([1.0/rw,1.0/rh,1.0/rw,1.0/rh,1.0/rw,1.0/rh,1.0/rw,1.0/rh])+np.array([displacements[l][0], displacements[l][1], displacements[l][0], displacements[l][1], displacements[l][0], displacements[l][1], displacements[l][0], displacements[l][1]])

					data_bb.append(box.tolist())

					cv2.polylines(frameDraw, [box[:8].reshape((-1, 1, 2))], True, color=(0, 255, 0), thickness=2)

				data_bb2.extend(data_bb)
				data_text2.extend(data_text)

			global_data["bbox"].append(data_bb2)
			global_data["text"].append(data_text2)

			cv2.imshow("text detection", frameDraw)
			key = cv2.waitKey(1)
			if key == 27:
				break

		with open(json_path+"_text_extraction.json", 'w') as handle:
			json.dump(global_data, handle)

		sess.close()

		cv2.destroyAllWindows()
		vs.release()

		return

if __name__ == '__main__':
	text_extraction(video_path=str(sys.argv[1]))