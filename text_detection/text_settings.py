import numpy as np
import cv2
import json
import sys


refPt = []
cropping = False

def text_settings(video_path=None):

	print(video_path)

	vs = cv2.VideoCapture(video_path)

	if vs.isOpened(): 
	    width = vs.get(3)
	    height = vs.get(4)
	 
	boxes_sel = []

	def click_and_crop(event, x, y, flags, param):
		global refPt, cropping

		if event == cv2.EVENT_LBUTTONDOWN:
			refPt = [x, y]
			cropping = True

		elif event == cv2.EVENT_LBUTTONUP:
			refPt.append(x)
			refPt.append(y)
			cropping = False
			cv2.rectangle(image, (refPt[0],refPt[1]), (refPt[2],refPt[3]), (0, 255, 0), 2)
			cv2.imshow("image", image)

	image = cv2.imread("select.jpg")
	image = cv2.resize(image, (int(width), int(height)))
	clone = image.copy()
	cv2.namedWindow("image")
	cv2.setMouseCallback("image", click_and_crop)
	 
	while True:
		cv2.imshow("image", image)
		key = cv2.waitKey(1) & 0xFF

		if key == ord("s"):
			image = clone.copy()
			boxes_sel.append(refPt)
			for rect in boxes_sel:
				cv2.rectangle(image, (rect[0],rect[1]), (rect[2],rect[3]), (0, 255, 0), 2)

		elif key == ord("d"):
			image = clone.copy()
			if len(boxes_sel)>0:
				boxes_sel.pop(-1)
				for rect in boxes_sel:
					cv2.rectangle(image, (rect[0],rect[1]), (rect[2],rect[3]), (0, 255, 0), 2)

		elif key == ord("q"):
			break

	cv2.destroyAllWindows()

	with open(video_path+"_text_settings.json", 'w') as handle:
		json.dump(boxes_sel, handle)


if __name__ == '__main__':
	text_settings(video_path = "../../"+str(sys.argv[1]))