import numpy as np
import cv2
import json
import sys
import os

from gtts import gTTS
#import enchant
from pygame import mixer
import pygame


def text_presentation(video_path=None):

	path = video_path.split("/")
	name_json_video = path[-1].split(".")[0]
	json_path = ""
	for i, p in enumerate(path):
		if i != len(path)-1:
			json_path += p
			json_path += "/"
		else:
			json_path += name_json_video
			break

	with open(json_path+"_text_process.json") as json_file:  
		final_sentences = json.load(json_file)

	pygame.init()

	cam = cv2.VideoCapture(video_path)
	frame_ind = 0

	ind_data = 0
	while True:

		ret_val, frame = cam.read()
		if not ret_val:
			break

		#if frame_ind == 0:
		#	img, (rh, rw) = resize_image(frame)

		cv2.imshow("Img", frame)
		key = cv2.waitKey(1)
		if key == 27:
			break

		st = False

		try:
			data = final_sentences[ind_data]

			if frame_ind == data[1][0]:
				st = True

				text = data[0]

				print(data[0])

				file = gTTS(text=text ,lang="ES")
				file.save(video_path+"_speech.mp3")
				pygame.mixer.init()

				SONG_END = pygame.USEREVENT + 1
				pygame.mixer.music.set_endevent(SONG_END)

				pygame.mixer.music.load(video_path+'_speech.mp3')
				pygame.mixer.music.play()

			if st:
				while True:
					for event in pygame.event.get():
						if event.type == SONG_END:
							st = False
							break
							print("the song ended!")

					if not st:
						ind_data += 1
						break

			frame_ind += 1

		except:
			frame_ind += 1
			continue

	cv2.destroyAllWindows()
	os.remove(video_path+"_speech.mp3")
	cam.release()


if __name__ == '__main__':
	text_presentation(video_path="../../"+str(sys.argv[1]))