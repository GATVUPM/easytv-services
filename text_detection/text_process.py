import json
import cv2
import numpy as np
import pickle
import math
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
import time
import sys, os

from gtts import gTTS
#import enchant
from pygame import mixer
import pygame

from difflib import SequenceMatcher
import collections


def get_images():
    files = []
    exts = ['jpg', 'png', 'jpeg', 'JPG']
    for parent, dirnames, filenames in os.walk(FLAGS.test_data_path):
        for filename in filenames:
            for ext in exts:
                if filename.endswith(ext):
                    files.append(os.path.join(parent, filename))
                    break
    #print('Find {} images'.format(len(files)))
    return files


def resize_image(img):
    img_size = img.shape
    im_size_min = np.min(img_size[0:2])
    im_size_max = np.max(img_size[0:2])

    im_scale = float(600) / float(im_size_min)
    if np.round(im_scale * im_size_max) > 1200:
        im_scale = float(1200) / float(im_size_max)
    new_h = int(img_size[0] * im_scale)
    new_w = int(img_size[1] * im_scale)

    new_h = new_h if new_h // 16 == 0 else (new_h // 16 + 1) * 16
    new_w = new_w if new_w // 16 == 0 else (new_w // 16 + 1) * 16

    re_im = cv2.resize(img, (new_w, new_h), interpolation=cv2.INTER_LINEAR)
    return re_im, (new_h / img_size[0], new_w / img_size[1])


def bb_iou(boxA, boxB):
	# determine the (x, y)-coordinates of the intersection rectangle
	xA = max(boxA[0], boxB[0])
	yA = max(boxA[1], boxB[1])
	xB = min(boxA[2], boxB[2])
	yB = min(boxA[3], boxB[3])

	# compute the area of intersection rectangle
	interArea = max(0, xB - xA + 1) * max(0, yB - yA + 1)

	# compute the area of both the prediction and ground-truth
	# rectangles
	boxAArea = (boxA[2] - boxA[0] + 1) * (boxA[3] - boxA[1] + 1)
	boxBArea = (boxB[2] - boxB[0] + 1) * (boxB[3] - boxB[1] + 1)

	# compute the intersection over union by taking the intersection
	# area and dividing it by the sum of prediction + ground-truth
	# areas - the interesection area
	iou = interArea / float(boxAArea + boxBArea - interArea)

	# return the intersection over union value
	return iou


def bb_intersection_over_union(boxA, boxB):
	# determine the (x, y)-coordinates of the intersection rectangle
	xA = max(boxA[0], boxB[0])
	yA = max(boxA[1], boxB[1])
	xB = min(boxA[2], boxB[2])
	yB = min(boxA[3], boxB[3])
 
	# compute the area of intersection rectangle
	interArea = max(0, xB - xA + 1) * max(0, yB - yA + 1)
 
	# compute the area of both the prediction and ground-truth
	# rectangles
	boxAArea = (boxA[2] - boxA[0] + 1) * (boxA[3] - boxA[1] + 1)
	boxBArea = (boxB[2] - boxB[0] + 1) * (boxB[3] - boxB[1] + 1)
 
	# compute the intersection over union by taking the intersection
	# area and dividing it by the sum of prediction + ground-truth
	# areas - the interesection area
	iou = interArea / float(boxAArea + boxBArea - interArea)
 
	# return the intersection over union value
	return iou


def text_process(video_path=None):

	print("Text Process")

	path = video_path.split("/")
	name_json_video = path[-1].split(".")[0]
	json_path = ""
	for i, p in enumerate(path):
		if i != len(path)-1:
			json_path += p
			json_path += "/"
		else:
			json_path += name_json_video
			break

	with open(json_path+"_text_extraction.json") as json_file:  
		data = json.load(json_file)

	cam = cv2.VideoCapture(video_path)

	frame_ind = 0

	final_data = []
	current_data = []
	frames_coin = []
	text_data = []
	recortes = []

	while True:
		ret_val, frame = cam.read()
		if not ret_val:
			break

		#if frame_ind == 0:
		#	img, (rh, rw) = resize_image(frame)

		try:
			boxes = data["bbox"][frame_ind]
			text = data["text"][frame_ind]
		except:
			frame_ind += 1
			continue

		#print(len(data["bbox"]), len(text))

		#print(boxes)

		if current_data == [] and len(boxes)>0:
			for k, box in enumerate(boxes):
				current_data.append([[box[0],box[1],box[4],box[5]]])
				frames_coin.append([frame_ind])
				text_data.append([text[k]])
				recortes.append(frame[box[1]-5:box[5]+5,box[0]-5:box[4]+5])

		elif len(boxes)==0 and len(current_data)>0:

			for j,box_cur in enumerate(current_data):
				final_data.append([current_data[j],frames_coin[j],text_data[j]])

			current_data = []
			frames_coin = []
			text_data = []

		else:
			matches = []

			current_data_cp = current_data[:]

			#print(len(current_data), len(boxes))

			for j,box_cur in enumerate(current_data):
				
				for l, box in enumerate(boxes):

					iou = bb_iou([box[0],box[1],box[4],box[5]], box_cur[-1])

					if iou>0.5:
						current_data_cp[j].append([box[0],box[1],box[4],box[5]])
						frames_coin[j].append(frame_ind)
						text_data[j].append(text[l])
						matches.append(j)

			#print(current_data_cp, frames_coin, matches)

			new_current = []
			new_frames = []
			new_text = []
			for j,box_cur in enumerate(current_data_cp):
				if j not in matches:
					final_data.append([current_data_cp[j],frames_coin[j],text_data[j]])
				else:
					new_current.append(box_cur)
					new_frames.append(frames_coin[j])
					new_text.append(text_data[j])

			frames_coin = new_frames[:]
			current_data = new_current[:]
			text_data = new_text[:]

			for k, box in enumerate(boxes):
				check_new = []
				for cur_box in current_data:

					if box[0]==cur_box[-1][0] and box[1]==cur_box[-1][1] and box[4]==cur_box[-1][2] and box[5]==cur_box[-1][3]:
						check_new.append(0)
					else:
						check_new.append(1)

				if not 0 in check_new:
					current_data.append([[box[0],box[1],box[4],box[5]]])
					frames_coin.append([frame_ind])
					text_data.append([text[k]])
					recortes.append(frame[box[1]-5:box[5]+5,box[0]-5:box[4]+5])


		#for box in boxes:
		#	cv2.rectangle(frame, (box[0]-5, box[1]-5), (box[4]+5, box[5]+5), (0,255,00), 2, 1)

		#print(frame_ind)
		frame_ind += 1

		#cv2.imshow("Img", frame)
		#key = cv2.waitKey(1)
		#if key == 27:
		#	break

	for j,box_cur in enumerate(current_data):
		final_data.append([current_data[j],frames_coin[j],text_data[j]])

	cv2.destroyAllWindows()
	cam.release()

	print(final_data)

	#with open(file_inter, 'wb') as handle:
	#	pickle.dump(final_data, handle, protocol=pickle.HIGHEST_PROTOCOL)

	#with open(file_inter, 'rb') as handle:
	#	final_data = pickle.load(handle)

	#######################################################################
	'''for box in final_data:

		if box[1][-1]-box[1][0]<3000:
			continue

		cam = cv2.VideoCapture(video_path)
		frame_ind = 0

		print(box[1][0], box[1][-1])

		while True:

			ret_val, frame = cam.read()
			if not ret_val:
				break

			if frame_ind == 0:
				img, (rh, rw) = resize_image(frame)

			try:
				if frame_ind in box[1]:
					ind = box[1].index(frame_ind)
					#cv2.rectangle(frame, (box[0][ind][0], box[0][ind][1]), (box[0][ind][2], box[0][ind][3]), (0,255,00), 2, 1)
			except:
				pass

			if frame_ind > box[1][-1]:
				break

			if frame_ind >= box[1][0]:

				print(box[2][frame_ind-box[1][0]])

				cv2.imshow("Img", frame)
				key = cv2.waitKey(1)
				if key == 27:
					break

			frame_ind += 1

		cv2.destroyAllWindows()
		cam.release()'''
	#########################################################################

	final_data2 = []

	#dictio = enchant.Dict("es_ES")

	for i, box in enumerate(final_data):
		try:
			c = CountVectorizer()
			bow_matrix = c.fit_transform(np.array(box[2]))
			normalized_matrix = TfidfTransformer().fit_transform(bow_matrix)
			similarity_graph = normalized_matrix * normalized_matrix.T
			text_mat = similarity_graph.toarray()
			conf = np.sum(np.sum(text_mat, axis=1), axis=0)/text_mat.shape[0]**2

			#print(text_mat.shape, conf, normalized_matrix.shape )

			#if text_mat.shape[0]>6000:
			#cv2.imshow("Recorte", recortes[i])
			#	text_map_op = text_mat
			#	print(np.sum(normalized_matrix)/(normalized_matrix.shape[1]*normalized_matrix.shape[0]))
			#cv2.waitKey(0)
		except:
			continue

		if box[1][-1]-box[1][0]<50:
			continue

		if conf>0.3:

			'''total_count = 0
			true_count = 0
			for sent in box[2]:
				for word in sent.split():
					res = dictio.check(word)
					total_count += 1
					if res:
						true_count += 1

			if true_count/total_count>0.3:'''

			final_data2.append(box)

			print("Bien")

		else:
			if text_mat.shape[0]>25:

				print("Mal")

				'''total_count = 0
				true_count = 0
				for sent in box[2]:
					print(sent)
					for word in sent.split():
						res = dictio.check(word)
						total_count += 1
						if res:
							true_count += 1

				if true_count/total_count>0.5:'''
				if len(box[0])>150:
					box.append([1])
				
					frases = []
					ant = []
					interval = 25
					index = 0

					final_phrase = []
					all_phrases = []

					for r, sent in enumerate(box[2]):
						if r == 0 or r%interval==0:
							sent = sent.split()
							ind_coin = []

							for i,sen in enumerate(sent):
								if i>5:
									break

								for j,an in enumerate(ant):
									if j>5:
										break
									if sen == an and j==i:
										ind_coin.append(j)

							if len(ind_coin)>0:
								sent_new = sent[ind_coin[-1]+2:-1]
							else:
								sent_new = sent[1:-1]

							all_phrases.append(sent_new)

							ant = sent
							index += 1

							if len(sent_new)>2:

								fin = False
								if len(final_phrase)==0:
									final_phrase = sent_new
								else:	
									for i in range(len(final_phrase)):
										if fin:
											break
										for j, word in enumerate(sent_new):
											#print(SequenceMatcher(None, word, final_phrase[-1-i]).ratio(), word, final_phrase[-1-i])

											print(i, len(final_phrase))
											if SequenceMatcher(None, word, final_phrase[-1-i]).ratio()>0.85:


												if i>0:
													for l in range(i):
														final_phrase.pop(-1)
												for n, s in enumerate(sent_new):
													if j+1 > n:
														continue
													final_phrase.append(s)
													fin = True

													#print(final_phrase)
													#cv2.imshow("Img", np.zeros((3,3,3)))
													#cv2.waitKey(0)

												break

					phrase = ""
					for w in final_phrase:
						phrase += w+" "

					jump = 100
					first_words = phrase[:jump]

					coin = [0,0]
					for i in range(len(phrase)):
						if i < jump:
							continue

						c = CountVectorizer()
						bow_matrix = c.fit_transform([first_words, phrase[i:i+jump]])
						normalized_matrix = TfidfTransformer().fit_transform(bow_matrix)
						similarity_graph = normalized_matrix * normalized_matrix.T
						text_mat = similarity_graph.toarray()
						conf = np.sum(np.sum(text_mat, axis=1), axis=0)/text_mat.shape[0]**2

						if conf>coin[0]:
							coin[0] = conf
							coin[1] = i

					if coin[0]>0.9:
						phrase = phrase[:coin[1]]

					for i in range(len(box[2])):
						box[2][i] = phrase

					#word_counts = collections.Counter(phrase.split(" "))
					#greater_word = ""
					#index = -1
					#for word, count in sorted(word_counts.items()):
					#	print('"%s" is repeated %d time%s.' % (word, count, "s" if count > 1 else ""))
					#	if count > index:
					#		index = count
					#		greater_word = word

				final_data2.append(box)


			'''cam = cv2.VideoCapture(name_tel)
			frame_ind = 0

			print(box[1][0], box[1][-1])

			while True:

				ret_val, frame = cam.read()
				if not ret_val:
					break

				if frame_ind == 0:
					img, (rh, rw) = resize_image(frame)

				try:
					if frame_ind in box[1]:
						ind = box[1].index(frame_ind)
						cv2.rectangle(frame, (math.ceil(box[0][ind][0]*(1.0/rw)-5), math.ceil(box[0][ind][1]*(1.0/rh)-5)), (math.ceil(box[0][ind][2]*(1.0/rw)+5), math.ceil(box[0][ind][3]*(1.0/rh)+5)), (0,255,00), 2, 1)
				except:
					pass

				if frame_ind > box[1][-1]:
					break

				if frame_ind >= box[1][0]:

					print(box[2][frame_ind-box[1][0]])

					cv2.imshow("Img", frame)
					key = cv2.waitKey(1)
					if key == 27:
						break

				frame_ind += 1

			cv2.destroyAllWindows()
			cam.release()'''


	#final_data2 = final_data[:]

	coin_text = []
	all_stored = []
	for i, box1 in enumerate(final_data2):
		if box1 not in all_stored:
			matches = []
			matches.append(box1)
			all_stored.append(box1)

			for j, box2 in enumerate(final_data2):
				if i == j or box2 in all_stored:
					continue

				init1 = box1[1][0]
				fin1 = box1[1][-1]
				init2 = box2[1][0]
				fin2 = box2[1][-1]

				#print(init1,init2,fin1,fin2)

				if init1>=init2:
					maxi = init1
				else:
					maxi = init2

				if fin1<=fin2:
					mini = fin1
				else:
					mini = fin2

				if mini-maxi>50:
					matches.append(box2)
					all_stored.append(box2)

			coin_text.append(matches)


	from collections import Counter


	new_data_times = []
	for i, mat in enumerate(coin_text):
		maxi = []
		mini = []
		y_val = []
		for box in mat:
			maxi.append(box[1][-1])
			mini.append(box[1][0])
			inter = 0
			for b in box[0]:
				inter += b[1]

			y_val.append(int(inter/len(box[0])))

		indsort = np.argsort(np.array(y_val))

		t_max = int(np.min(np.array(maxi)))
		t_min = int(np.max(np.array(mini)))

		sentences_ordered = []

		for j in range(len(indsort)):
			#sentences_ordered.append(mat[indsort[j]][2])


			#### Process text
			#mat[indsort[j]][2]
			c = Counter(mat[indsort[j]][2])
			#print(c.most_common(1)[0][0])
			sentences_ordered.append(c.most_common(1)[0][0])
			#time.sleep(5)

		#print(sentences_ordered)

		new_data_times.append([sentences_ordered, [t_min, t_max]])

	#####################################################################################
	for i,boxes in enumerate(coin_text):
		if boxes[0][1][-1]-boxes[0][1][0]<50:
			continue

		cam = cv2.VideoCapture(video_path)
		frame_ind = 0

		while True:

			ret_val, frame = cam.read()
			if not ret_val:
				break

			#if frame_ind == 0:
			#	img, (rh, rw) = resize_image(frame)

			for box in boxes:

				if frame_ind in box[1]:
					#print(box[1][0], box[1][-1])
					ind = box[1].index(frame_ind)
					cv2.rectangle(frame, (box[0][ind][0]-5, box[0][ind][1]-5), (box[0][ind][2]+5, box[0][ind][3]+5), (0,255,00), 2, 1)
					print(box[2][frame_ind-box[1][0]])

			#if frame_ind > boxes[i][1][-1]:
			#	break

			cv2.imshow("Img", frame)
			key = cv2.waitKey(25)
			if key == 27:
				break

			frame_ind += 1

		cv2.destroyAllWindows()
		cam.release()

	#####################################################################################

	# Process sentences nlp

	import string
	import nltk
	#nltk.download('punkt')

	from nltk.tokenize import word_tokenize

	final_sentences = []
	anter = []
	for sent in new_data_times:
		final_sent = ""

		#if len(sent)>2:
		#	print(sent[0])

		for sen in sent[0]:

			proc = str.maketrans('', '', string.punctuation)
			stripped = [w.translate(proc) for w in sen]

			#print(stripped)

			strip_copy = stripped[:]
			for i, strip in enumerate(strip_copy):
				if strip == '' or strip == " ":
					stripped.pop(0)
				else:
					break
			#print(stripped)

			mid = ""
			for strip in stripped:
				mid += strip
			
			if len(stripped) < 6:
				continue

			#char_ok = True 
			#for part in word_tokenize(mid):
			#	char_ok = part.isalpha()
			#	if not char_ok:
			#		break

			strip_copy = stripped[:]
			for i, let in enumerate(strip_copy):
				if not let.isalpha() and let != " ":
					try:
						ent = int(let)
					except:
						stripped[i] = " "

			#if not char_ok:
			#	continue

			if mid in anter:
				continue

			anter.append(mid)

			for strip in stripped:
				final_sent += strip

			final_sent += ". "

		#time.sleep(1)
		if final_sent != "":
			final_sentences.append([final_sent, sent[1]])


	with open(json_path+"_text_process.json", 'w') as handle:
		json.dump(final_sentences, handle)

	'''pygame.init()

	st = False
	for i,data in enumerate(new_data_times):

		cam = cv2.VideoCapture(name_tel)
		frame_ind = 0

		while True:

			ret_val, frame = cam.read()
			if not ret_val:
				break

			if frame_ind == 0:
				img, (rh, rw) = resize_image(frame)


			if frame_ind == data[1][0]:
				st = True

				text = ""
				for t in data[0]:
					text += t + " "

				file = gTTS(text=text ,lang="ES")
				file.save("speech.mp3")
				pygame.mixer.init()

				SONG_END = pygame.USEREVENT + 1
				pygame.mixer.music.set_endevent(SONG_END)

				pygame.mixer.music.load('speech.mp3')
				pygame.mixer.music.play()

			if st:

				cv2.imshow("Img", frame)
				key = cv2.waitKey(1)
				if key == 27:
					break

				for event in pygame.event.get():

					if event.type == SONG_END:
						st = False
						break
						print("the song ended!")

			frame_ind += 1'''

	cv2.destroyAllWindows()
	try:
		cam.release()
	except:
		pass

if __name__ == '__main__':
	text_process(video_path="../www/media/"+str(sys.argv[1]))