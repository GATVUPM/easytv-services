# -*- coding: utf-8 -*-
import os
from helper.custom_log import init_logger
from enum import Enum

import tensorflow as tf

# ====================================================================================
# ------------------------- GLOBAL VARIABLES PREPROCESSING ---------------------------
# ====================================================================================


logger = None
log_file_name =None


# ----------------------------------- Spech GLobal variables --------------
os.environ['media_dir'] = os.path.join('www', 'media')
os.environ['audio_dir'] = os.path.join('Dataset', 'audio')

# --------------------------------------------------------------------------
os.environ['trained_models_path'] = os.path.join('output', 'saved_models')
os.environ['speaker_names_dict'] = os.path.join('output', 'speaker_names.json')


segmenter = None
episode_dir = None
serie_dir = None
sp_model_dir = None
xy_dir = None
audio_dataframe = None
speaker_diarizaton_status = None
output_filename_labels = None
sd = None
model_sp = None
sp_model_name = None
sp_weights_name = None
# -------------------------------------------------------------------------


def load_speaker_diar_model():
    try:
        global model_sp
        if audio_output_dir is not None and sp_model_name is not None and sp_weights_name is not None:
            loaded_model_dir = os.path.join(audio_output_dir, sp_model_name)

            with open(loaded_model_dir, 'r') as json_file:
                architecture = json.load(json_file)
                model_sp = model_from_json(json.dumps(architecture))

            # load weights into new model
            weights_dir = os.path.join(audio_output_dir, sp_weights_name)
            model_sp.load_weights(weights_dir)
            logger.info('Speaking Diarization Deep Learning model loaded with success!')
    except Exception as e:
        msg = 'Unable to load the Deep Learning model. Error description: ' +  str(e)
        logger.error(msg)


def load_segmenter_model():
    try:
        global segmenter
        segmenter = Segmenter()
        logger.info('Voice Activity Detector Model loaded with success!')
    except Exception as e:
        msg = 'It was not possible to load the Voice Activity Detector Model. Error: ' + str(e)
        logger.error(msg)


def init_speaker_diarization():
    global speaker_diarizaton_status
    global xy_dir
    global sd
    global audio_output_dir
    global output_filename_labels
    global sp_model_name
    global sp_weights_name
	
    speaker_diarizaton_status = {}
    output_filename_labels = 'labels_encode.json'
    sp_model_name = 'speakDirNet.json'
    sp_weights_name = 'speakDirNet_weights.h5'



class Status(Enum):
	NONPREPROCESSED = 0
	PREPROCESSING = 1
	ANNOTATED = 2
	WAITINGUSERANOTATION = 3
	GENERATINGANOTATIONFILE = 4
	PROCESSED = 5
	PREPROCESSED = 6
	PROCESSING = 7

# ============================================================
# ------------------Init Gloval variables --------------------
# ============================================================
def init():
	global logger
	global log_file_name
	global proccesing
	global video_status
	global text_status

	global graph
	
	video_status={}
	text_status={}
	proccesing=False
	logger = init_logger(__name__, testing_mode=False)
	log_file_name =  os.path.join("logs","app.log")
	init_speaker_diarization()

	graph = tf.get_default_graph()
	