# -*- coding: utf-8 -*-
import os
import tensorflow as tf
from helper.custom_log import init_logger
from enum import Enum
import multiprocessing


# ====================================================================================
# ------------------------- GLOBAL VARIABLES PREPROCESSING ---------------------------
# ====================================================================================


logger = None
log_file_name =None
sound_service = None


class Status(Enum):

    NONPREPROCESSED = 0
    PREPROCESSING =  1
    ANNOTATED =  2
    WAITINGUSERANOTATION = 3
    GENERATINGANOTATIONFILE =  4
    PROCESSED =  5
    PREPROCESSED = 6
    PROCESSING = 7

# ============================================================
# ------------------Init Gloval variables --------------------
# ============================================================


def init():
    global logger
    global log_file_name
    global proccesing
    global video_status
    global text_status

    global graph

    global model_faces
    global modelface
    global mark_detector
    global APIKEYS

    manager = multiprocessing.Manager()
    APIKEYS=["vdxv8kZfTJhZ8P5p78uK8pMyyMqTvBUrHGqLGRb6bto=r"]
    video_status=manager.dict()
    text_status={}
    proccesing = multiprocessing.Value('i', 0)
    logger = init_logger(__name__, testing_mode=False)
    log_file_name = os.path.join("logs","app.log")

    graph = tf.get_default_graph()
