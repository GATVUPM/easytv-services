import numpy as np
import imutils
import time
import cv2
import random
import json

# Networks
from keras.applications.resnet50 import ResNet50, preprocess_input
from keras.layers import Input
from keras.layers import GlobalAveragePooling2D, Dense, Dropout, Dense, Activation, Flatten, Dropout
from keras.models import Sequential, Model
from keras.models import load_model

# Utils
import matplotlib.pyplot as plt
import random, glob
import os, sys, csv
import time, datetime
import math
import uuid

import tensorflow as tf
graph = tf.get_default_graph()

def face_characters(video_path=None):

	global graph
	with graph.as_default():

		path = video_path.split("/")
		init_path = ""
		for i, p in enumerate(path):
			if i != len(path)-2:
				init_path += p
				init_path += "/"
			else:
				break

		init_path2 = ""
		for i, p in enumerate(path):
			if i != len(path)-1:
				init_path2 += p
				init_path2 += "/"
			else:
				break

		name_json_video = path[-1].split(".")[0]
		json_path = ""
		for i, p in enumerate(path):
			if i != len(path)-1:
				json_path += p
				json_path += "/"
			else:
				json_path += name_json_video
				break

		model_name = init_path+'ResNet50_model_weights.h5'

		'''image_input = Input(shape=(224, 224, 3))
		base_model = ResNet50(input_tensor=image_input, include_top=True, weights='imagenet')
		x=base_model.output
		x=Dense(1024,activation='relu')(x) #we add dense layers so that the model can learn more complex functions and classify for better results.
		preds=Dense(2,activation='softmax')(x) #final layer with softmax activation
		model=Model(inputs=base_model.input,outputs=preds)
		model.summary()

		model.load_weights(model_name)'''

		model = load_model(model_name)
		model.summary()

		#with open(json_path+"_characters_annotations.json") as json_file:  
		#	annotations = json.load(json_file)

		with open(json_path+"_faces_process.json") as json_file:  
			data_c = json.load(json_file)

		characters = []
		for _, dirnames, filenames in os.walk(init_path+'Dataset/images'):
			for name in dirnames:
				characters.append(name)

		def iou_bb(boxA, boxB):
			# determine the (x, y)-coordinates of the intersection rectangle
			xA = max(boxA[0], boxB[0])
			yA = max(boxA[1], boxB[1])
			xB = min(boxA[2], boxB[2])
			yB = min(boxA[3], boxB[3])


			# compute the area of intersection rectangle
			interArea = max(0, xB - xA + 1) * max(0, yB - yA + 1)

			# compute the area of both the prediction and ground-truth
			# rectangles
			boxAArea = (boxA[2] - boxA[0] + 1) * (boxA[3] - boxA[1] + 1)
			boxBArea = (boxB[2] - boxB[0] + 1) * (boxB[3] - boxB[1] + 1)

			# compute the intersection over union by taking the intersection
			# area and dividing it by the sum of prediction + ground-truth
			# areas - the interesection area
			iou = interArea / float(boxAArea + boxBArea - interArea)

			# return the intersection over union value
			return iou


		def most_frequent(List): 
			return max(set(List), key = List.count) 

		n_frames = len(data_c["faces"])
		scene_len = 0
		for i in data_c["scene_change"]:
		    if i == 1:
		        scene_len += 1

		for i in range(scene_len):

		    total_faces = []
		    for l, d in enumerate(data_c["faces"]):
		        if len(list(d.keys()))==0:
		            continue

		        if int(list(d.keys())[0]) == i:
		            total_faces.append([list(d.values()), l, list(d.keys())[0]])

		        elif int(list(d.keys())[0])>i:
		            break

		    final_faces = []
		    for face in total_faces:
		        for c, f in enumerate(face[0]):

		            if len(final_faces)==0:
		                final_faces.append([[f],[face[1]], face[2]])
		            else:
		                no_match = 0
		                ff_cp = final_faces[:]
		                for ind, fc in enumerate(final_faces):
		                    iou = iou_bb([fc[0][-1]["x"], fc[0][-1]["y"], fc[0][-1]["x"]+fc[0][-1]["w"], fc[0][-1]["y"]+fc[0][-1]["h"]],[f["x"], f["y"], f["x"]+f["w"], f["y"]+f["h"]])
		                    if iou>0.6:
		                        ff_cp[ind][0].append(f)
		                        ff_cp[ind][1].append(face[1])
		                        ff_cp[ind][2] = face[2]
		                    else:
		                        no_match += 1

		                if no_match == len(final_faces):
		                    final_faces.append([[f],[face[1]], face[2]])

		    cam = cv2.VideoCapture(video_path)
		    fps = cam.get(cv2.CAP_PROP_FPS)
		    ind_frame = 0
		    paths_img = []

		    if not os.path.exists(init_path2+'Video_Frames'):
		        os.makedirs(init_path2+'Video_Frames')

		        while True:
		            ret_val, frame = cam.read()
		            if not ret_val:
		                break
		            cv2.imwrite(init_path2+"Video_Frames/"+str(ind_frame).zfill(7)+".jpg", frame)
		            paths_img.append(init_path2+"Video_Frames/"+str(ind_frame).zfill(7)+".jpg")
		            ind_frame += 1


		    paths_img = []
		    for root, dirs, files in os.walk(init_path2+"Video_Frames/"):
		        paths_img = sorted(files)

		    for final_face in final_faces:

		        frames_in = final_face[1]
		        fcut = []

		        names = []

		        for frame_ind, frame_p in enumerate(paths_img[frames_in[0]:frames_in[-1]+1]):
		            frame_ind2 = frame_ind+frames_in[0]
		            frame = cv2.imread(init_path2+"Video_Frames/"+frame_p)

		            #cv2.rectangle(frame, (final_face[0][frame_ind2-frames_in[0]]["x"], final_face[0][frame_ind2-frames_in[0]]["y"]), (final_face[0][frame_ind2-frames_in[0]]["x"]+final_face[0][frame_ind2-frames_in[0]]["w"], final_face[0][frame_ind2-frames_in[0]]["y"]+final_face[0][frame_ind2-frames_in[0]]["h"]), (0,0,255), 2, 1)
		            
		            #y = int(final_face[0][frame_ind2-frames_in[0]]["y"]) - 10 if int(final_face[0][frame_ind2-frames_in[0]]["y"]) - 10 > 10 else int(final_face[0][frame_ind2-frames_in[0]]["y"]) + 10
		            #cv2.putText(frame, "Speaking: " + str(final_face[0][frame_ind2-frames_in[0]]["speak"][0]), (int(final_face[0][frame_ind2-frames_in[0]]["x"]), int(y)), cv2.FONT_HERSHEY_SIMPLEX, 0.45, (0,0,255), 2)

		            #cv2.imshow("Frame", frame)
		            #cv2.waitKey(10)

		            try:
			            h0, w0, _ = frame.shape

			            y00 = final_face[0][frame_ind2-frames_in[0]]["y"]
			            if 0 > y00:
			            	y00 = 0
			            y11 = final_face[0][frame_ind2-frames_in[0]]["y"]+final_face[0][frame_ind2-frames_in[0]]["h"]
			            if y11 > h0-1:
			            	y11 = h0-1
			            x00 = final_face[0][frame_ind2-frames_in[0]]["x"]
			            if 0 > x00:
			            	x00 = 0
			            x11 = final_face[0][frame_ind2-frames_in[0]]["x"]+final_face[0][frame_ind2-frames_in[0]]["w"]
			            if x11 > w0-1:
			            	x11 = w0-1

			            fcut.append(frame[y00:y11,x00:x11])
			            #print("cutting")

			            image_p =  np.expand_dims(cv2.resize(fcut[-1]/255, (224, 224)), axis=0)
			            
			            #name = characters[np.argmax(model.predict(image_p)[0])]
			            name = model.predict(image_p)[0].tolist()
			            names.append(name)

		            except:
			            #names.append("Unknown")

			            name = [1.0/len(characters)] * len(characters)
			            #name[-1] = 1
			            names.append(name)

		            if frame_ind2==frames_in[-1]:
		                #final_name = most_frequent(names)
		                #print(final_name)

		                for ind1, d1 in enumerate(data_c["faces"]):
		                    ind_ex = 0
		                    for indi, d2 in enumerate(list(d1.keys())):
		                        if d2 == final_face[2]:
		                            #data_c["faces"][ind1][d2]["name"] = final_name
		                            data_c["faces"][ind1][d2]["name"] = names[ind_ex]
		                            ind_ex += 1

		            else:
		                if frame_ind2>frames_in[-1]:
		                    break
		                else:
		                    #frame_ind += 1
		                    pass

		        cv2.destroyAllWindows()
		        cam.release()

		print("Finished "+ json_path)
		with open(json_path+"_faces_final.json", 'w') as outfile:
		    json.dump(data_c, outfile)

		#sys.exit()
		return

if __name__ == '__main__':
	face_characters(video_path="../../video_data/test/test.mp4")