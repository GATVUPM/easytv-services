from imutils.video import VideoStream
import numpy as np
import argparse
import imutils
import time
import cv2
import random
from pathlib import Path
import json
import pickle
import math
import matplotlib.pyplot as plt
import random, glob
import os, sys, csv
import time, datetime
import math
import os
import uuid
import moviepy.editor as mp


def face_extraction(video_path=None):

	path = video_path.split("/")
	init_path = ""
	for i, p in enumerate(path):
		if i != len(path)-2:
			init_path += p
			init_path += "/"
		else:
			break

	init_path2 = ""
	for i, p in enumerate(path):
		if i != len(path)-1:
			init_path2 += p
			init_path2 += "/"
		else:
			break

	name_json_video = path[-1].split(".")[0]
	json_path = ""
	for i, p in enumerate(path):
		if i != len(path)-1:
			json_path += p
			json_path += "/"
		else:
			json_path += name_json_video
			break

	with open(json_path+"_characters_annotations.json") as json_file:  
		annotations = json.load(json_file)

	with open(json_path+"_faces_process.json") as json_file:  
		data_c = json.load(json_file)

	with open(json_path+"_faces_annotator.json") as json_file:  
		frames_annotator = json.load(json_file)

	characters = []
	for name in annotations:
		if name not in characters and name != None:
			characters.append(name)

	if not os.path.exists(init_path+'Dataset'):
		os.makedirs(init_path+'Dataset')

	if not os.path.exists(init_path+'Dataset/images'):
		os.makedirs(init_path+'Dataset/images')

	if not os.path.exists(init_path+'Dataset/audio'):
		os.makedirs(init_path+'Dataset/audio')

	for character in characters:
		if not os.path.exists(init_path+'Dataset/images/'+character):
			os.makedirs(init_path+'Dataset/images/'+character)

		if not os.path.exists(init_path+'Dataset/audio/'+character):
			os.makedirs(init_path+'Dataset/audio/'+character)


	def iou_bb(boxA, boxB):
		# determine the (x, y)-coordinates of the intersection rectangle
		xA = max(boxA[0], boxB[0])
		yA = max(boxA[1], boxB[1])
		xB = min(boxA[2], boxB[2])
		yB = min(boxA[3], boxB[3])

		# compute the area of intersection rectangle
		interArea = max(0, xB - xA + 1) * max(0, yB - yA + 1)

		# compute the area of both the prediction and ground-truth
		# rectangles
		boxAArea = (boxA[2] - boxA[0] + 1) * (boxA[3] - boxA[1] + 1)
		boxBArea = (boxB[2] - boxB[0] + 1) * (boxB[3] - boxB[1] + 1)

		# compute the intersection over union by taking the intersection
		# area and dividing it by the sum of prediction + ground-truth
		# areas - the interesection area
		iou = interArea / float(boxAArea + boxBArea - interArea)

		# return the intersection over union value
		return iou

	def getOverlap(a, b):
		return max(0, min(a[1], b[1]) - max(a[0], b[0]))

	n_frames = len(data_c["faces"])
	scene_len = 0
	for i in data_c["scene_change"]:
	    if i == 1:
	        scene_len += 1

	json_data = []
	json_data_2 = {"frames": [], "fps": 0}

	for i in range(scene_len):

	    total_faces = []
	    for l, d in enumerate(data_c["faces"]):
	        if len(list(d.keys()))==0:
	            continue

	        if int(list(d.keys())[0]) == i:
	            total_faces.append([list(d.values()), l])

	        elif int(list(d.keys())[0])>i:
	            break

	    final_faces = []
	    for face in total_faces:
	        for c, f in enumerate(face[0]):

	            if len(final_faces)==0:
	                final_faces.append([[f],[face[1]]])
	            else:
	                no_match = 0
	                ff_cp = final_faces[:]
	                for ind, fc in enumerate(final_faces):
	                    iou = iou_bb([fc[0][-1]["x"], fc[0][-1]["y"], fc[0][-1]["x"]+fc[0][-1]["w"], fc[0][-1]["y"]+fc[0][-1]["h"]],[f["x"], f["y"], f["x"]+f["w"], f["y"]+f["h"]])
	                    if iou>0.75:
	                        ff_cp[ind][0].append(f)
	                        ff_cp[ind][1].append(face[1])
	                    else:
	                        no_match += 1

	                if no_match == len(final_faces):
	                    final_faces.append([[f],[face[1]]])

	    cam = cv2.VideoCapture(video_path)
	    fps = cam.get(cv2.CAP_PROP_FPS)
	    ind_frame = 0
	    paths_img = []

	    if not os.path.exists(init_path2+'Video_Frames'):
	        os.makedirs(init_path2+'Video_Frames')

	        while True:
	            ret_val, frame = cam.read()
	            if not ret_val:
	                break
	            cv2.imwrite(init_path2+"Video_Frames/"+str(ind_frame).zfill(7)+".jpg", frame)
	            paths_img.append(init_path2+"Video_Frames/"+str(ind_frame).zfill(7)+".jpg")
	            ind_frame += 1


	    paths_img = []
	    for root, dirs, files in os.walk(init_path2+"Video_Frames/"):
	        paths_img = sorted(files)

	    for indface, final_face in enumerate(final_faces):

	        json_data.append(final_face)
	        
	        json_data_2["frames"].append([final_face[1][0],final_face[1][-1]])

	        over_frames = []
	        ids_frames = []
	        for ind, pair_frames in enumerate(frames_annotator["frames"]):
	        	overlap = getOverlap([final_face[1][0],final_face[1][-1]], pair_frames)
	        	over_frames.append(overlap)
	        	ids_frames.append(ind)

	        if max(over_frames) == 0:
	        	continue

	        ind_over = np.argmax(np.array(over_frames))
	        ids_frames = ids_frames[ind_over]
	        name = annotations[ids_frames]
	        if name == None:
	        	continue

	        frames_in = final_face[1]

	        fcut = []

	        for frame_ind, frame_p in enumerate(paths_img[frames_in[0]:frames_in[-1]+1]):
	            frame_ind2 = frame_ind+frames_in[0]
	            frame = cv2.imread(init_path2+"Video_Frames/"+frame_p)

	            #cv2.rectangle(frame, (final_face[0][frame_ind2-frames_in[0]]["x"], final_face[0][frame_ind2-frames_in[0]]["y"]), (final_face[0][frame_ind2-frames_in[0]]["x"]+final_face[0][frame_ind2-frames_in[0]]["w"], final_face[0][frame_ind2-frames_in[0]]["y"]+final_face[0][frame_ind2-frames_in[0]]["h"]), (0,0,255), 2, 1)
	            
	            #y = int(final_face[0][frame_ind2-frames_in[0]]["y"]) - 10 if int(final_face[0][frame_ind2-frames_in[0]]["y"]) - 10 > 10 else int(final_face[0][frame_ind2-frames_in[0]]["y"]) + 10
	            #cv2.putText(frame, "Speaking: " + str(final_face[0][frame_ind2-frames_in[0]]["speak"][0]), (int(final_face[0][frame_ind2-frames_in[0]]["x"]), int(y)), cv2.FONT_HERSHEY_SIMPLEX, 0.45, (0,0,255), 2)

	            #cv2.imshow("Frame", frame)
	            #cv2.waitKey(5)
	            try:
		            h0, w0, _ = frame.shape

		            y00 = final_face[0][frame_ind2-frames_in[0]]["y"]
		            if 0 > y00:
		            	y00 = 0
		            y11 = final_face[0][frame_ind2-frames_in[0]]["y"]+final_face[0][frame_ind2-frames_in[0]]["h"]
		            if y11 > h0-1:
		            	y11 = h0-1
		            x00 = final_face[0][frame_ind2-frames_in[0]]["x"]
		            if 0 > x00:
		            	x00 = 0
		            x11 = final_face[0][frame_ind2-frames_in[0]]["x"]+final_face[0][frame_ind2-frames_in[0]]["w"]
		            if x11 > w0-1:
		            	x11 = w0-1

		            fcut.append(frame[y00:y11,x00:x11])
		            #print("cutting")
	            except:
		            break

	            if frame_ind2==frames_in[-1]:

	                uuide = uuid.uuid4()

	                sec1 = frames_in[0]%fps
	                milsec1 = (frames_in[0]-sec1*fps)/fps

	                sec2 = frames_in[-1]%fps
	                milsec2 = (frames_in[-1]-sec2*fps)/fps

	                print(sec1+milsec1,sec2+milsec2)

	                clip = mp.VideoFileClip(video_path).subclip(sec1+milsec1,sec2+milsec2)
	                try:
	                    clip.audio.write_audiofile(init_path+'Dataset/audio/'+name+"/"+str(uuide)+"_"+str(frame_ind)+".wav")
	                except:
	                    break

	                indf = 0
	                for img in fcut:
	                    cv2.imwrite(init_path+'Dataset/images/'+name+"/"+str(uuide)+"_"+str(indf).zfill(7)+".jpg", img)
	                    indf += 1
	                
	            # Cache problems (edited)    
	                try:
	                    clip.close()
	                except:
	                    break
	            #frame_ind += 1
	            else:
	                if frame_ind2>frames_in[-1]:
	                    break
	                else:
	                    #frame_ind += 1
	                    pass

	        cv2.destroyAllWindows()
	        cam.release()

	for character in characters:
		if len(os.listdir(init_path+'Dataset/images/'+character))==0:
			os.rmdir(init_path+'Dataset/images/'+character)
		if len(os.listdir(init_path+'Dataset/audio/'+character))==0:
			os.rmdir(init_path+'Dataset/audio/'+character)

	print("Data Extraction Complete!")


if __name__ == '__main__':
	face_extraction(video_path="../../video_data/test/test.mp4")
