# filter warnings
import warnings
warnings.simplefilter(action="ignore", category=FutureWarning)

import tensorflow as tf

# keras imports
from keras.applications.vgg16 import VGG16, preprocess_input
from keras.applications.vgg19 import VGG19, preprocess_input
from keras.applications.xception import Xception, preprocess_input
from keras.applications.resnet50 import ResNet50, preprocess_input
from keras.applications.inception_resnet_v2 import InceptionResNetV2, preprocess_input
from keras.applications.mobilenet import MobileNet, preprocess_input
from keras.applications.inception_v3 import InceptionV3, preprocess_input
from keras.layers import Input
from keras.preprocessing.image import ImageDataGenerator
from keras import optimizers 
from keras import models
from keras import layers

# other imports
import numpy as np
import os
from sklearn.metrics import confusion_matrix

graph = tf.get_default_graph()


def face_training(video_path=None):

    global graph
    with graph.as_default():

        path = video_path.split("/")
        init_path = ""
        for i, p in enumerate(path):
            if i != len(path)-2:
                init_path += p
                init_path += "/"
            else:
                break

        folders = 0
        for _, dirnames, filenames in os.walk(init_path+'Dataset/images_v2/M'):
            folders += len(dirnames)
            
        # config variables
        batch_size = 32

        image_input = Input(shape=(224, 224, 3))
        resnet_conv = ResNet50(input_tensor=image_input, include_top=False, weights='imagenet')
        #vgg_conv = VGG16(weights='imagenet', include_top=False, input_shape=(224, 224, 3))
        #for layer in vgg_conv.layers[:-4]:
        #    layer.trainable = False

        # Create the model
        model = models.Sequential()
        # Add the vgg convolutional base model
        model.add(resnet_conv)
        # Add new layers
        model.add(layers.Flatten())
        model.add(layers.Dense(1024, activation='relu'))
        model.add(layers.Dropout(0.5))
        model.add(layers.Dense(1024, activation='relu'))
        model.add(layers.Dropout(0.5))
        model.add(layers.Dense(folders, activation='softmax'))
         
        # Show a summary of the model. Check the number of trainable parameters
        model.summary()

        print("[INFO] successfully loaded base model and model...")

        datagen = ImageDataGenerator(validation_split=0.2, rescale=1./255, shear_range=0.2, zoom_range=0.2, horizontal_flip=True, width_shift_range=0.2, height_shift_range=0.2, fill_mode='nearest')
        TRAIN_DIR = init_path+'Dataset/images_v2/M'

        train_generator = datagen.flow_from_directory(TRAIN_DIR, subset='training', batch_size=batch_size, target_size=(224, 224))
        val_generator = datagen.flow_from_directory(TRAIN_DIR, subset='validation', batch_size=batch_size, target_size=(224, 224))

        filepath = init_path+'Dataset/model_faces_m.h5'
        #checkpoint = ModelCheckpoint(filepath, monitor='val_loss', verbose=1, save_best_only=True,save_weights_only=False, mode='min', period=1)
        #callbacks_list = [checkpoint]

        # Compile the model
        model.compile(loss='categorical_crossentropy',
                      optimizer=optimizers.SGD(lr=1e-3),
                      metrics=['acc'])
        # Train the model
        history = model.fit_generator(
              train_generator,
              steps_per_epoch=train_generator.samples/train_generator.batch_size ,
              epochs=10,
              validation_data=val_generator,
              validation_steps=val_generator.samples/val_generator.batch_size,
              verbose=1)
        
        print('Storing Faces Model!')
        # Save the model
        model.save(filepath)

        Y_pred = model.predict_generator(val_generator, val_generator.samples/val_generator.batch_size)
        y_pred = np.argmax(Y_pred, axis=1)
        cm = confusion_matrix(val_generator.classes, y_pred)
        np.save(init_path+'Dataset/confusion_matrix_image_m.npy', cm)
        
        ###########################################################3
        folders = 0
        for _, dirnames, filenames in os.walk(init_path+'Dataset/images_v2/F'):
            folders += len(dirnames)
            
        # config variables
        batch_size = 32

        image_input = Input(shape=(224, 224, 3))
        resnet_conv = ResNet50(input_tensor=image_input, include_top=False, weights='imagenet')
        #vgg_conv = VGG16(weights='imagenet', include_top=False, input_shape=(224, 224, 3))
        #for layer in vgg_conv.layers[:-4]:
        #    layer.trainable = False

        # Create the model
        model = models.Sequential()
        # Add the vgg convolutional base model
        model.add(resnet_conv)
        # Add new layers
        model.add(layers.Flatten())
        model.add(layers.Dense(1024, activation='relu'))
        model.add(layers.Dropout(0.5))
        model.add(layers.Dense(1024, activation='relu'))
        model.add(layers.Dropout(0.5))
        model.add(layers.Dense(folders, activation='softmax'))
         
        # Show a summary of the model. Check the number of trainable parameters
        model.summary()

        print("[INFO] successfully loaded base model and model...")

        datagen = ImageDataGenerator(validation_split=0.2, rescale=1./255, shear_range=0.2, zoom_range=0.2, horizontal_flip=True, width_shift_range=0.2, height_shift_range=0.2, fill_mode='nearest')
        TRAIN_DIR = init_path+'Dataset/images_v2/F'

        train_generator = datagen.flow_from_directory(TRAIN_DIR, subset='training', batch_size=batch_size, target_size=(224, 224))
        val_generator = datagen.flow_from_directory(TRAIN_DIR, subset='validation', batch_size=batch_size, target_size=(224, 224))

        filepath = init_path+'Dataset/model_faces_f.h5'
        #checkpoint = ModelCheckpoint(filepath, monitor='val_loss', verbose=1, save_best_only=True,save_weights_only=False, mode='min', period=1)
        #callbacks_list = [checkpoint]

        # Compile the model
        model.compile(loss='categorical_crossentropy',
                      optimizer=optimizers.SGD(lr=1e-3),
                      metrics=['acc'])
        # Train the model
        history = model.fit_generator(
              train_generator,
              steps_per_epoch=train_generator.samples/train_generator.batch_size ,
              epochs=10,
              validation_data=val_generator,
              validation_steps=val_generator.samples/val_generator.batch_size,
              verbose=1)
        
        print('Storing Faces Model!')
        # Save the model
        model.save(filepath)

        Y_pred = model.predict_generator(val_generator, val_generator.samples/val_generator.batch_size)
        y_pred = np.argmax(Y_pred, axis=1)
        cm = confusion_matrix(val_generator.classes, y_pred)
        np.save(init_path+'Dataset/confusion_matrix_image_f.npy', cm)

        

if __name__ == '__main__':
    face_training(video_path="../../video_data/comsifos/test/test.mp4")