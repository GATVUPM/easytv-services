import json
import numpy as np
import cv2
import math
import analytic_wfm
import os
import time, datetime
import uuid


def face_process(video_path=None):

    print("Faces process")

    # Model face if exists
    path = video_path.split("/")
    init_path = ""
    for i, p in enumerate(path):
        if i != len(path)-2:
            init_path += p
            init_path += "/"
        else:
            break

    path = video_path.split("/")
    name_json_video = path[-1].split(".")[0]
    json_path = ""
    for i, p in enumerate(path):
        if i != len(path)-1:
            json_path += p
            json_path += "/"
        else:
            json_path += name_json_video
            break

    with open(json_path+"_faces_extraction.json") as json_file:  
        all_data = json.load(json_file)

    data_c = all_data["d1"]
    data = all_data["d2"]

    draw_detections = True

    clave_final = False

    keys = list(data.keys())
    for i, vals in enumerate(data.values()):
        if vals == []:
            continue

        age_f = []
        for n in vals["age"]:
            if n == None:
                continue
            age_f.append(n)
        age_f = np.array(age_f).mean()

        gender_f = np.array([0,0])
        for n in vals["gender"]:
            if n == None:
                continue
            if n == "M":
                gender_f[0] += 1
            elif n == "F":
                gender_f[1] += 1

        gender_f = np.argmax(gender_f)

        '''if vals["name"][0] != "-1":
            clave_final = True
            names = []
            for n in vals["name"]:
                names.append(n)
            name = max(set(names), key = names.count)
        else:
            name = str(uuid.uuid4())'''

        model_name = init_path+'Dataset/model_faces_m.h5'
        if os.path.isfile(model_name):
            clave_final = True

        name = str(uuid.uuid4())

        for j, l in enumerate(data_c["faces"]):
            if keys[i] in list(l.keys()):
                isNan = math.isnan(age_f)
                valu = None
                if isNan:
                    valu = 30
                else:
                    valu = int(age_f)
                data_c["faces"][j][keys[i]]["age"] = valu
                data_c["faces"][j][keys[i]]["gender"] = int(gender_f)
                data_c["faces"][j][keys[i]]["name2"] = name

    def Nmaxelements(list1, N): 
        final_list = [] 
      
        for i in range(0, N):  
            max1 = -9999999999999999999999
              
            for j in range(len(list1)):      
                if list1[j] > max1: 
                    max1 = list1[j]; 
                      
            list1.remove(max1); 
            final_list.append(max1) 
              
        return final_list 

    keys = list(data.keys())
    scene_changes = []
    for i,s in enumerate(data_c["scene_change"]):
        if s == 1:
            scene_changes.append(i)

    colors = ["r", "b", "g", "m", "p", "r", "b", "g", "m", "p", "r", "b", "g", "m", "p"]
    ind_color = 0
    #if draw_detections:
        #plt.figure()
        
    app_json = {"scene_changes": [], "characters": [], "speaking": []}
        
    for c,change in enumerate(scene_changes):
        
        if change == 1:
            app_json["scene_changes"].append(c)
        
        coincidence = []
        keys_track = list(data.keys())
        for i, vals in enumerate(data.values()):

            isInChange = False
            if c != len(scene_changes)-1:
                for n in range(change,scene_changes[c+1]):
                    if n in vals["nf"]:
                        isInChange = True
                        break

            else:
                for n in range(change,len(data_c["faces"])):
                    if n in vals["nf"]:
                        isInChange = True
                        break

            #if change in vals["nf"]:
            if isInChange:
                m = []
                f = []
                for k,n in enumerate(np.array(vals["keypoints"])):
                    if type(n) is np.ndarray:
                        #diff1 = np.sqrt((n[51][0]-n[57][0])**2+(n[51][1]-n[57][1])**2)/vals["h"][k]
                        #diff2 = np.sqrt((n[50][0]-n[58][0])**2+(n[50][1]-n[58][1])**2)/vals["h"][k]
                        #diff3 = np.sqrt((n[52][0]-n[56][0])**2+(n[52][1]-n[56][1])**2)/vals["h"][k]
                        diff1 = np.sqrt((n[51][1]-n[57][1])**2)/vals["h"][k]
                        diff2 = np.sqrt((n[50][1]-n[58][1])**2)/vals["h"][k]
                        diff3 = np.sqrt((n[52][1]-n[56][1])**2)/vals["h"][k]
                        diff4 = np.sqrt((n[49][1]-n[59][1])**2)/vals["h"][k]
                        diff5 = np.sqrt((n[53][1]-n[55][1])**2)/vals["h"][k]
                        diff6 = np.sqrt((n[33][1]-n[57][1])**2)/vals["h"][k]

                        diff7 = np.sqrt((n[62][1]-n[66][1])**2)/vals["h"][k]
                        #diff1 = (n[51][1]-n[57][1])**2/vals["h"][k]
                        #diff2 = (n[50][1]-n[58][1])**2/vals["h"][k]
                        #diff3 = (n[52][1]-n[56][1])**2/vals["h"][k]
                        #diff4 = (n[49][1]-n[59][1])**2/vals["h"][k]
                        #diff5 = (n[53][1]-n[55][1])**2/vals["h"][k]
                        #diff = diff1+diff2+diff3+diff4+diff5+diff6+diff7
                        diff = [diff1,diff2,diff3,diff4,diff5,diff6,diff7]
                        if len(m)==0:
                            m.append(diff)
                            f.append(vals["nf"][k])
                        else:
                            if vals["nf"][k] == f[-1]+1:
                                m.append(diff)
                                f.append(vals["nf"][k])

                #plt.plot(f,m, colors[ind_color])
                diff_ant = []
                for r,ms in enumerate(m):
                    if r == 0:
                        diff_ant.append(0.0)
                        continue
                    #diff_ant.append(ms-m[r-1])
                    diff_ant.append(np.sum(np.abs(np.subtract(np.array(ms), np.array(m[r-1])))))
                
                maxPeaks, minPeaks = analytic_wfm.peakdetect(diff_ant, lookahead = 1, delta = 0.05)
                if draw_detections:
                    '''for rr in range(len(f)):
                        for jj in maxPeaks: 
                            if rr == jj[0]:
                                #plt.plot(f[rr],diff_ant[rr],'go')

                    for rr in range(len(f)):
                        for jj in minPeaks: 
                            if rr == jj[0]:
                                #plt.plot(f[rr],diff_ant[rr],'go')'''

                    #plt.plot(f,diff_ant, colors[ind_color])

                ind_color += 1

                coincidence.append([diff_ant, f, maxPeaks, minPeaks, keys_track[i]])

        if c != len(scene_changes)-1:
            partitions = []
            cut_p = 75
            part = []
            for t in range(scene_changes[c+1]-change):
                part.append(change+t)
                if len(part)==cut_p or t == scene_changes[c+1]-change-1:
                    partitions.append(part)
                    part = []

        else:
            partitions = []
            cut_p = 75
            part = []
            for t in range(len(data_c["faces"])-change):
                part.append(change+t)
                if len(part)==cut_p or t == len(data_c["faces"])-change-1:
                    partitions.append(part)
                    part = []

        for part in partitions:
            for coin in coincidence:
                val = []
                fns = []
                for p,fr_n in enumerate(coin[1]):
                    if fr_n in part:
                        val.append(coin[0][p])
                        fns.append(fr_n)
                if len(val) in list(range(6)):
                    for g in fns:
                        for jj, ll in enumerate(data_c["faces"]):
                            if jj == g and str(coin[4]) in list(ll.keys()):
                                isSp = 0
                                argSp = 0.0
                                data_c["faces"][jj][str(coin[4])]["speak"] = [isSp, argSp]
                                data_c["faces"][jj][str(coin[4])].pop("keypoints")
                                break
                else:
                    ind_min = np.array(val).argsort()[:5]
                    minVals = []
                    for g in ind_min.tolist():
                        minVals.append(val[g])
                    maxVals = Nmaxelements(val, 5)

                    diff_speak = np.array(maxVals).mean()-np.array(minVals).mean()

                    if draw_detections:
                        up_line = []
                        down_line = []
                        for g in range(len(fns)):
                            up_line.append(np.median(val))
                            down_line.append(np.median(val))
                        #plt.plot(fns,up_line, "g")
                        #plt.plot(fns,down_line, "g")

                    for g in fns:
                        for jj, ll in enumerate(data_c["faces"]):
                            if jj == g and str(coin[4]) in list(ll.keys()):
                                isSp = 0
                                argSp = 0.0
                                #print(diff_speak)
                                if diff_speak>0.075:
                                    isSp = 1
                                    argSp = diff_speak

                                data_c["faces"][jj][str(coin[4])]["speak"] = [isSp, argSp]
                                data_c["faces"][jj][str(coin[4])].pop("keypoints")
                                break

        ind_color = 0

    for i,l in enumerate(data_c["faces"]):
        for key in list(l.keys()):
            if "keypoints" in data_c["faces"][i][key]:
                data_c["faces"][i].pop(key)

    for d in data_c["faces"]:
        for p in d.values():
            p.pop('keypoints', None)

    with open(json_path+"_faces_process.json", 'w') as outfile:
        json.dump(data_c, outfile)

    if clave_final:
        with open(json_path+"_faces_final.json", 'w') as outfile:
            json.dump(data_c, outfile)

    '''if draw_detections:
        #plt.show()
        #plt.ion()
        cam = cv2.VideoCapture(video_path)
        frame_ind = 0
        while True:
            ret_val, frame = cam.read()
            if not ret_val:
                break

            faces = data_c["faces"][frame_ind]
            for f in faces.values():
                cv2.rectangle(frame, (f["x"], f["y"]), (f["x"]+f["w"], f["y"]+f["h"]), (0,0,255), 2, 1)
                age_d = ""
                gender_d = ""
                age_d = str(f["age"])
                gender_d = "M" if f["gender"] == 0 else "F"
                y = int(f["y"]) - 10 if int(f["y"]) - 10 > 10 else int(f["y"]) + 10
                cv2.putText(frame, "-1 " + age_d + " " + gender_d + " " + str(f["speak"][0]), (int(f["x"]), y), cv2.FONT_HERSHEY_SIMPLEX, 0.45, (0,0,255), 2)

            cv2.imshow("img", frame)
            cv2.waitKey(33)

            frame_ind += 1

        cv2.destroyAllWindows()
        cam.release()'''


    json_data = {"fps": 0, "frames": []}

    frame_ind = 1
    video = cv2.VideoCapture(video_path)
    fps = video.get(cv2.CAP_PROP_FPS)
    json_data["fps"] = fps
    speaking = []
    while True:

        ret_val, frame = video.read()
        if not ret_val:
            break

        faces = data_c["faces"][frame_ind-1]
        speak = False
        name = None
        for f in faces.values():
            if f["speak"][0] == 1:
                speak = True
                name = f["name2"]

                xs = f['x']
                ys = f['y']
                ws = f['w']
                hs = f['h']

                speaking.append([speak, name, frame_ind, xs, ys, ws, hs])

        if (speaking!=[] and speak==False) or (speaking!=[] and name!=speaking[0][1]):

            if speaking[-1][2]-speaking[0][2]>=fps:

                json_data["frames"].append([speaking[0][2], speaking[-1][2], speaking[0][3], speaking[0][4], speaking[0][5], speaking[0][6]])

            if speaking!=[] and speak==False:
                speaking = []

            elif speaking!=[] and name!=speaking[0][1]:
                speaking = [speaking[-1]]

        frame_ind += 1

    with open(json_path+"_faces_annotator.json", 'w') as outfile:
        json.dump(json_data, outfile)

    print("Analysis and json extraction complete")
    return

if __name__ == '__main__':
    face_process(video_path="../../video_data/test/test.mp4")