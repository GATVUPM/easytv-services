from imutils.video import VideoStream
import numpy as np
import imutils
import time
import cv2
import dlib
from imutils import face_utils
from .munkres import Munkres
import random
from pathlib import Path
from contextlib import contextmanager
from .wide_resnet import WideResNet
import json
import pickle
import math
import matplotlib.pyplot as plt
import random, glob
import os, sys, csv

import time, datetime
from .utils import *
import uuid

# Networks
from keras.preprocessing import image
from keras.applications.resnet50 import ResNet50
from keras.applications.vgg16 import VGG16 as VGG16n
from keras.applications.vgg19 import VGG19
from keras.applications.inception_v3 import InceptionV3
from keras.applications.xception import Xception
from keras.applications.inception_resnet_v2 import InceptionResNetV2
from keras.applications.mobilenet import MobileNet
from keras.applications.densenet import DenseNet121, DenseNet169, DenseNet201
from keras.applications.nasnet import NASNetLarge, NASNetMobile
from keras.preprocessing.image import ImageDataGenerator
from keras.preprocessing import image
from keras.layers import GlobalAveragePooling2D, Dense, Dropout, Dense, Activation, Flatten, Dropout
from keras import backend as K
from keras.models import Model
from keras.preprocessing.image import load_img
from keras.preprocessing.image import img_to_array
from keras import optimizers
from keras import losses
from keras.optimizers import SGD, Adam
from keras.models import Sequential, Model
from keras.callbacks import ModelCheckpoint, LearningRateScheduler
from keras.models import load_model
from keras.utils.data_utils import get_file

import scenedetect
from scenedetect.video_manager import VideoManager
from scenedetect.scene_manager import SceneManager
from scenedetect.frame_timecode import FrameTimecode
from scenedetect.stats_manager import StatsManager
from scenedetect.detectors import ContentDetector

from .mark_detector import MarkDetector

from helper import global_variables as gv

import tensorflow as tf
graph = tf.get_default_graph()


def face_detection(video_path=None):
	from multiprocessing import Process, Queue
	global graph
	with graph.as_default():

		abs_path = os.path.abspath(os.path.dirname(__file__))+"/"

		draw_detections = True
		use_scene_det = True
		track_sel = 'KCF'

		if use_scene_det:
			video_manager = VideoManager([video_path])
			stats_manager = StatsManager()
			scene_manager = SceneManager(stats_manager)
			scene_manager.add_detector(ContentDetector())
			base_timecode = video_manager.get_base_timecode()

			scene_changes = []
			try:
			    video_manager.set_duration()
			    video_manager.set_downscale_factor()
			    video_manager.start()
			    scene_manager.detect_scenes(frame_source=video_manager)
			    scene_list = scene_manager.get_scene_list(base_timecode)

			    for i, scene in enumerate(scene_list):
			    	scene_changes.append(int(scene[0].get_frames()))

			finally:
			    video_manager.release()

		# Trackers
		trackerTypes = ['BOOSTING', 'MIL', 'KCF','TLD', 'MEDIANFLOW', 'GOTURN', 'MOSSE', 'CSRT']

		def createTrackerByName(trackerType):
		  # Create a tracker based on tracker name
		  if trackerType == trackerTypes[0]:
		    tracker = cv2.TrackerBoosting_create()
		  elif trackerType == trackerTypes[1]: 
		    tracker = cv2.TrackerMIL_create()
		  elif trackerType == trackerTypes[2]:
		    tracker = cv2.TrackerKCF_create()
		  elif trackerType == trackerTypes[3]:
		    tracker = cv2.TrackerTLD_create()
		  elif trackerType == trackerTypes[4]:
		    tracker = cv2.TrackerMedianFlow_create()
		  elif trackerType == trackerTypes[5]:
		    tracker = cv2.TrackerGOTURN_create()
		  elif trackerType == trackerTypes[6]:
		    tracker = cv2.TrackerMOSSE_create()
		  elif trackerType == trackerTypes[7]:
		    tracker = cv2.TrackerCSRT_create()
		  else:
		    tracker = None
		    print('Incorrect tracker name')
		    print('Available trackers are:')
		    for t in trackerTypes:
		      print(t)
		    
		  return tracker


		# Model face if exists
		path = video_path.split("/")
		init_path = ""
		for i, p in enumerate(path):
			if i != len(path)-2:
				init_path += p
				init_path += "/"
			else:
				break

		name_json_video = path[-1].split(".")[0]
		json_path = ""
		for i, p in enumerate(path):
			if i != len(path)-1:
				json_path += p
				json_path += "/"
			else:
				json_path += name_json_video
				break

		model_name = init_path+'ResNet50_model_weights.h5'

		if os.path.isfile(model_name):
			model_faces = load_model(model_name)
			#model_faces.summary()
			for _, dirnames, filenames in os.walk(init_path+'Dataset/images'):
				characters = []
				for ele in sorted(dirnames): 
					characters.append(ele)

				#characters = list(dirnames)
				break

		# Model age gender recognition
		pretrained_model = "https://github.com/yu4u/age-gender-estimation/releases/download/v0.5/weights.28-3.73.hdf5"
		modhash = 'fbe63257a054c1c5466cfd7bf14646d6'

		depth = 16
		k = 8
		weight_file = None
		margin = 0.4

		if not weight_file:
			weight_file = get_file("weights.28-3.73.hdf5", pretrained_model, cache_subdir="pretrained_models", file_hash=modhash, cache_dir=Path(__file__).resolve().parent)
		# load model and weights
		img_size = 64
		modelface = WideResNet(img_size, depth=depth, k=k)()
		modelface.load_weights(weight_file)

		# initialize the video stream and allow the cammera sensor to warmup
		cam = cv2.VideoCapture(video_path)

		# association
		munkresAlg = Munkres()
		fgbg = cv2.createBackgroundSubtractorMOG2(history=5, varThreshold=200, detectShadows=False)

		# landmark detector
		mark_detector = MarkDetector()

		# Params
		frame_n = -1
		success = True

		bbox = []
		ind_track = 0
		trackers = {}
		colors = {}
		names = {}
		labels = {}
		actual_n = 0

		trackers_fin = {}
		n_p = 0
		faces_cut = {}

		boxes = []
		loss_flag = False

		multiTracker = cv2.MultiTracker_create()

		final_complete_info = {"scene_change": [], "faces": []}
		final_complete_info2 = {}

		def non_max_suppression_fast(boxes, overlapThresh):
			if len(boxes) == 0:
				return []

			if boxes.dtype.kind == "i":
				boxes = boxes.astype("float")
		 
			pick = []
			x1 = boxes[:,0]
			y1 = boxes[:,1]
			x2 = boxes[:,0]+boxes[:,2]
			y2 = boxes[:,1]+boxes[:,3]
		 
			area = (x2 - x1 + 1) * (y2 - y1 + 1)
			idxs = np.argsort(y2)
		 
			while len(idxs) > 0:

				last = len(idxs) - 1
				i = idxs[last]
				pick.append(i)

				xx1 = np.maximum(x1[i], x1[idxs[:last]])
				yy1 = np.maximum(y1[i], y1[idxs[:last]])
				xx2 = np.minimum(x2[i], x2[idxs[:last]])
				yy2 = np.minimum(y2[i], y2[idxs[:last]])
		 
				w = np.maximum(0, xx2 - xx1 + 1)
				h = np.maximum(0, yy2 - yy1 + 1)

				overlap = (w * h) / area[idxs[:last]]
				idxs = np.delete(idxs, np.concatenate(([last],
					np.where(overlap > overlapThresh)[0])))
		 
			return boxes[pick].astype("int")


		def get_face(detector, img_queue, box_queue):
			while True:
				image = img_queue.get()
				if image[1]:
					break
				box = detector.extract_cnn_facebox(image[0])
				box_queue.put(box)

		frame_ant = None

		print("Starting Faces Detection Process")

		while True:

			ret_val, frame = cam.read()
			if not ret_val:
				img_queue.put([frame_ant, True])
				break

			frame_ant = frame.copy()

			if frame_n == -1:
				img_queue = Queue()
				box_queue = Queue()
				#kill_queue = Queue()
				img_queue.put([frame, False])
				box_process = Process(target=get_face, args=(mark_detector, img_queue, box_queue))
				box_process.start()

			else:
				img_queue.put([frame, False])

			frame_n += 1

			frame_draw = frame.copy()
			(h, w) = frame.shape[:2]

			# Face detector
			faceboxes = box_queue.get()

			boxToDraw = []
			if faceboxes is not None:
				boxToDraw = faceboxes

			matrix_munk = []
			indexes = []

			fgmask = fgbg.apply(frame)
			if ((use_scene_det and frame_n in scene_changes) or (len(boxToDraw)>0 and trackers=={})) or ((np.mean(fgmask)>75 or (len(boxToDraw)>0 and trackers=={})) and not use_scene_det):

				ind_track = 0
				trackers = {}
				colors = {}
				names = {}
				labels = {}

				if (not use_scene_det and np.mean(fgmask)>75) or (use_scene_det and frame_n in scene_changes) or not success:
					trackers_fin = {}
					multiTracker = cv2.MultiTracker_create()

				bbox = boxToDraw[:]
				for bb in bbox:

					startX, startY, endX, endY = bb
					
					trackers[str(ind_track)] = [bb]
					colors[str(ind_track)] = (int(random.random() * 255), int(random.random() * 255), int(random.random() * 255))
					ind_track += 1

					if (not use_scene_det and not np.mean(fgmask)>75) or (use_scene_det and frame_n not in scene_changes):
						isAnt = False
						for b in boxes:
							if np.sqrt(((startX+endX)/2-((b[2]+2*b[0])/2))**2+((startY+endY)/2-((b[3]+2*b[1])/2))**2)<100:
								isAnt = True
								break
						if not isAnt:
							multiTracker.add(createTrackerByName(track_sel), frame, (startX,startY,endX-startX, endY-startY))
							trackers_fin[str(n_p)] = [[startX,startY,endX-startX, endY-startY]]

							#faces_cut[str(n_p)] = [frame[startY:endY,startX:endX]]

							n_p += 1
					else:
						multiTracker.add(createTrackerByName(track_sel), frame, (startX,startY,endX-startX, endY-startY))
						trackers_fin[str(n_p)] = [[startX,startY,endX-startX, endY-startY]]

						#faces_cut[str(n_p)] = [frame[startY:endY,startX:endX]]

						n_p += 1

			elif len(bbox) != 0 and len(boxToDraw) != 0:
				matrix_munk = np.zeros((len(boxToDraw),len(bbox)))
				for i in range(len(boxToDraw)):
					for j in range(len(bbox)):
						n = np.sqrt(((boxToDraw[i][2]+boxToDraw[i][0])/2-((bbox[j][2]+bbox[j][0])/2))**2+((boxToDraw[i][3]+boxToDraw[i][1])/2-((bbox[j][3]+bbox[j][1])/2))**2)
						matrix_munk[i][j] = n

				indexes = munkresAlg.compute(matrix_munk.tolist())

				for i in range(len(boxToDraw)):
					isInd = False
					for indi in indexes:
						if indi[0] == i:
							if matrix_munk[indi[0],indi[1]]<50:
								keys = list(trackers.keys())
								for j, tracks in enumerate(trackers.values()):
									if tracks[-1][2] != 0:
										if tracks[-1][0] == bbox[indi[1]][0] and tracks[-1][1] == bbox[indi[1]][1] and tracks[-1][2] == bbox[indi[1]][2] and tracks[-1][3] == bbox[indi[1]][3]:
											trackers[keys[j]].append(boxToDraw[i])
											isInd = True
											break

							else:
								keys = list(trackers.keys())
								for j, tracks in enumerate(trackers.values()):
									if tracks[-1][0] == bbox[indi[1]][0] and tracks[-1][1] == bbox[indi[1]][1] and tracks[-1][2] == bbox[indi[1]][2] and tracks[-1][3] == bbox[indi[1]][3]:
										trackers[keys[j]].append(np.array([0,0,0,0]))
										break

							diff = 0
							for j, tracks in enumerate(trackers.values()):
								if tracks[-1][2]!=0:
									break
								else:
									diff += 1

							if diff == len(list(trackers.keys())):
								trackers = {}
								colors = {}
								names = {}
								labels = {}
								loss_flag = True

					if not isInd:
						trackers[str(ind_track)] = [boxToDraw[i]]
						colors[str(ind_track)] = (int(random.random() * 255), int(random.random() * 255), int(random.random() * 255))

						startX, startY, endX, endY = boxToDraw[i]
						
						isAnt = False
						for b in boxes:
							if np.sqrt(((endX+startX)/2-((b[2]+2*b[0])/2))**2+((endY+startY)/2-((b[3]+2*b[1])/2))**2)<100:
								isAnt = True
								break
						if not isAnt:
							multiTracker.add(createTrackerByName(track_sel), frame, (startX,startY,endX-startX, endY-startY))
							trackers_fin[str(n_p)] = [[startX,startY,endX-startX, endY-startY]]

							#faces_cut[str(n_p)] = [frame[startY:endY,startX:endX]]

							n_p += 1

						ind_track += 1

				if len(bbox)>len(boxToDraw):
					ind_in = []
					for indi in indexes:
						ind_in.append(indi[1])

					for i,b in enumerate(bbox):
						if i not in ind_in:
							keys = list(trackers.keys())
							for j, tracks in enumerate(trackers.values()):
								if tracks[-1][0] == b[0] and tracks[-1][1] == b[1] and tracks[-1][2] == b[2] and tracks[-1][3] == b[3]:
									trackers[keys[j]].append(np.array([0,0,0,0]))
									break

				bbox = boxToDraw[:]

			else:
				trackers = {}
				colors = {}
				names = {}
				labels = {}

			success, boxes = multiTracker.update(frame)
			if success == False:
				trackers = {}
				colors = {}
				names = {}
				labels = {}

			boxes_print = non_max_suppression_fast(boxes, 0.5)

			trackers_fin_copy = trackers_fin.copy()
			keys_track = list(trackers_fin.keys())
			trackers_fin = {}
			al_assoc = []
			for bb in boxes_print:
				for i,tracks in enumerate(trackers_fin_copy.values()):

					if np.sqrt(((2*tracks[-1][0]+tracks[-1][2])/2-((bb[2]+2*bb[0])/2))**2+((2*tracks[-1][1]+tracks[-1][3])/2-((bb[3]+2*bb[1])/2))**2)<100 and keys_track[i] not in al_assoc:
						#faces_cut[keys_track[i]].append(frame[startY:endY,startX:endX])
						#try:
						#	diff_faces = np.mean(cv2.resize(faces_cut[keys_track[i]][-1],(300,300))-cv2.resize(faces_cut[keys_track[i]][-2],(300,300)))
						#except:
						#	continue
						trackers_fin[keys_track[i]] = trackers_fin_copy[keys_track[i]]
						trackers_fin[keys_track[i]].append(bb)

						al_assoc.append(keys_track[i])
						break

			keys_track = list(trackers_fin.keys())
			faces_fin = {}

			for i, newbox in enumerate(trackers_fin.values()):
				p1 = (int(newbox[-1][0]), int(newbox[-1][1]))
				p2 = (int(newbox[-1][0] + newbox[-1][2]), int(newbox[-1][1] + newbox[-1][3]))
				if draw_detections:
					cv2.rectangle(frame_draw, p1, p2, (0,0,255), 2, 1)

				faces_fin[keys_track[i]] = {"x": int(newbox[-1][0]), "y": int(newbox[-1][1]), "w": int(newbox[-1][2]), "h": int(newbox[-1][3]), "name": None, "age": None, "gender": None, "keypoints": None}
				
				if keys_track[i] not in list(final_complete_info2.keys()):
					final_complete_info2[keys_track[i]] = {"x": [int(newbox[-1][0])], "y": [int(newbox[-1][1])], "w": [int(newbox[-1][2])], "h": [int(newbox[-1][3])], "name": [None], "age": [None], "gender": [None], "keypoints": [None], "nf": [frame_n]}
				else:
					final_complete_info2[keys_track[i]]["x"].append(int(newbox[-1][0]))
					final_complete_info2[keys_track[i]]["y"].append(int(newbox[-1][1]))
					final_complete_info2[keys_track[i]]["w"].append(int(newbox[-1][2]))
					final_complete_info2[keys_track[i]]["h"].append(int(newbox[-1][3]))
					final_complete_info2[keys_track[i]]["name"].append(None)
					final_complete_info2[keys_track[i]]["age"].append(None)
					final_complete_info2[keys_track[i]]["gender"].append(None)
					final_complete_info2[keys_track[i]]["keypoints"].append(None)
					final_complete_info2[keys_track[i]]["nf"].append(frame_n)

				marks = None

				startX, startY, endX, endY = int(newbox[-1][0]), int(newbox[-1][1]), int(newbox[-1][0] + newbox[-1][2]), int(newbox[-1][1] + newbox[-1][3])

				gender = None
				age = None
				faces = np.empty((1, img_size, img_size, 3))
				xw1 = max(int(startX - margin * (endX-startX)), 0)
				yw1 = max(int(startY - margin * (endY-startY)), 0)
				xw2 = min(int(endX + margin * (endX-startX)), w - 1)
				yw2 = min(int(endY + margin * (endY-startY)), h - 1)
				faces[0, :, :, :] = cv2.resize(frame[yw1:yw2 + 1, xw1:xw2 + 1, :], (img_size, img_size))
				results = modelface.predict(faces)
				predicted_genders = results[0]
				ages = np.arange(0, 101).reshape(101, 1)
				predicted_ages = results[1].dot(ages).flatten()
				age = int(predicted_ages[0])
				gender = "F" if predicted_genders[0][0] > 0.5 else "M"

				faces_fin[keys_track[i]]["age"] = age
				faces_fin[keys_track[i]]["gender"] = gender

				final_complete_info2[keys_track[i]]["age"][-1] = age
				final_complete_info2[keys_track[i]]["gender"][-1] = gender

				if os.path.isfile(model_name):
					image_p =  np.expand_dims(cv2.resize(frame[yw1:yw2 + 1, xw1:xw2 + 1, :]/255, (224, 224)), axis=0)
					name_g2 = characters[np.argmax(model_faces.predict(image_p)[0])]
					name_g = model_faces.predict(image_p)[0].tolist()
					print(name_g2)
				else:
					name_g = "-1"

				faces_fin[keys_track[i]]["name"] = name_g
				final_complete_info2[keys_track[i]]["name"][-1] = name_g

				CNN_INPUT_SIZE = 64
				diff = 0
				if endY-startY>endX-startX:
					diff = (endY-startY)-(endX-startX)

				h0, w0, _ = frame.shape
				x00 = startX
				if x00<0:
					x00 = 0
				x11 = endX
				if x11>w0-1:
					x11 = w0-1
				y00 = startY+diff
				if y00<0:
					y00 = 0
				y11 = endY
				if y11>h0-1:
					y11 = h0-1

				face_img = frame[y00:y11,x00:x11]
				face_img = cv2.resize(face_img, (CNN_INPUT_SIZE, CNN_INPUT_SIZE))
				face_img = cv2.cvtColor(face_img, cv2.COLOR_BGR2GRAY)
				face_img0 = face_img.reshape(1, CNN_INPUT_SIZE, CNN_INPUT_SIZE, 1)
				marks = mark_detector.detect_marks_keras(face_img0)
				marks *= endX - startX
				marks[:, 0] += startX
				marks[:, 1] += startY+diff
				if draw_detections:
					for k, mark in enumerate(marks):
						cv2.circle(frame_draw, (int(mark[0]), int(mark[1])), 1, (255,255,255), -1, cv2.LINE_AA)

				faces_fin[keys_track[i]]["keypoints"] = marks.tolist()
				final_complete_info2[keys_track[i]]["keypoints"][-1] = marks.tolist()

				if draw_detections:
					age_d = str(age)
					gender_d = gender
					y = int(newbox[-1][1]) - 10 if int(newbox[-1][1]) - 10 > 10 else int(newbox[-1][1]) + 10
					cv2.putText(frame_draw,  "-1 " + age_d + " " + gender_d, (int(newbox[-1][0]), y), cv2.FONT_HERSHEY_SIMPLEX, 0.45, (0,0,255), 2)

			if draw_detections:
				cv2.imshow("Frame", frame_draw)
				key = cv2.waitKey(1) & 0xFF
				if key == ord("q"):
					break

			final_complete_info["scene_change"].append(1 if (use_scene_det and frame_n in scene_changes) or ((np.mean(fgmask)>75 and not use_scene_det)) else 0)
			final_complete_info["faces"].append(faces_fin)

		cv2.destroyAllWindows()
		cam.release()

		data_c = final_complete_info
		data = final_complete_info2

		final_json = {"d1": data_c, "d2": data}

		with open(json_path+"_faces_extraction.json", 'w') as handle:
			json.dump(final_json, handle)

		print("Faces extraction Finished!")


if __name__ == '__main__':
	face_detection(video_path="../../video_data/test/test.mp4")