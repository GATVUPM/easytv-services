#
#MPD EDITOR FOR EASY TV
#Developed by UPM
#02/06/2019
#

#Usage:
#python mpd_editor.py --mpd_path --service_tag --json_path

#Import necessary packages
import time
import shutil
import os


def mpd_editor(video_path=None, service=None):

	# Model face if exists
	path = video_path.split("/")
	init_path = ""
	for i, p in enumerate(path):
		if i != len(path)-1:
			init_path += p
			init_path += "/"
		else:
			break

	path = video_path.split("/")
	name_json_video = path[-1].split(".")[0]
	json_path2 = ""
	for i, p in enumerate(path):
		if i != len(path)-1 and i !=0 :
			json_path2 += p
			json_path2 += "/"
		else:
			json_path2 += name_json_video
			break

	if service == "face_detection":
		if os.path.isfile(json_path2+"_faces_final.json"):
			path_json = json_path2+"_faces_final.json"
		else:
			print("No file detected!")
			return
	elif service == "text_detection":
		path_json = json_path2+"_text_process.json"

	#Inputs
	mpd_path=init_path+"manifest_sbt.mpd"
	service_tag=service
	json_path=path_json

	print("--mpd_path: " +mpd_path)
	print("--service_tag: "+service_tag)
	print("--json_path: "+json_path)

	#Tags
	access_service_tag="access-services"
	mpd_tag="MPD"

	#Create a backup
	date=time.strftime("%Hh%Mm%Ss%d%m%Y") #24H format
	mpd_backup_path=mpd_path.split(".mpd")[0]+"_"+date+".mpd"
	shutil.copy(mpd_path,mpd_backup_path)
	print ("Backup of the MPD file created")
	    
	datafile=open(mpd_path,'r')
	found_tag=False
    
	for line in datafile:
		if access_service_tag in line:
			found_tag=True
	datafile.close()

	if found_tag == True:
		print("access_service_tag found in MPD")

		datafile=open(mpd_path,'r')
		text=datafile.read()
		text = text.replace("</"+access_service_tag+">", "<"+service_tag+">"+json_path+"</"+service_tag+">"+'\n'+"</"+access_service_tag+">" )
		datafile.close()

		datafile = open (mpd_path,'w')
		datafile.write(text)
		datafile.close()

		print("service_tag and json_path added")

	else:
		print("access_service_tag not found in MPD")
        
		datafile = open(mpd_path, 'r')
		text = datafile.read()
		text = text.replace("</"+mpd_tag+">", "<"+access_service_tag+">"+'\n'+"</"+access_service_tag+">"+'\n'+"</"+mpd_tag+">")
		datafile.close()

		datafile = open (mpd_path,'w')
		datafile.write(text)
		datafile.close()
		print("access_service_tag added")

		datafile = open(mpd_path, 'r')
		text = datafile.read()
		text = text.replace("</"+access_service_tag+">", "<"+service_tag+">"+json_path+"</"+service_tag+">"+'\n'+"</"+access_service_tag+">")
		datafile.close()

		datafile = open (mpd_path,'w')
		datafile.write(text)
		datafile.close()
		print("service_tag and json_path added")

	print("MPD created successfully")


