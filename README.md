# EASY_TV_UPM_SERVICES

Añadir los modelos

easytv-services/text_detection/checkpoints_mlt/ctpn_50000.ckpt.data-00000-of-00001

easytv-services/faces_service/landmark_model/Mobilenet_v1.hdf5

easytv-services/faces_service/face_detector_model/res10_300x300_ssd_iter_140000.caffemodel

easytv-services/faces_service/pretrained_models/weights.28-3.73.hdf5


Copyright 2020 Universidad Politécnica de Madrid

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.